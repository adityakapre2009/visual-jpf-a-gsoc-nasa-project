package edu.buffalo.cse.jive.internal.ui;

import static edu.buffalo.cse.jive.preferences.ImageInfo.IM_OM_CONTOUR_INSTANCE;
import static edu.buffalo.cse.jive.preferences.ImageInfo.IM_OM_CONTOUR_INTERFACE;
import static edu.buffalo.cse.jive.preferences.ImageInfo.IM_OM_CONTOUR_METHOD;
import static edu.buffalo.cse.jive.preferences.ImageInfo.IM_OM_CONTOUR_STATIC;
import static edu.buffalo.cse.jive.preferences.ImageInfo.IM_OM_CONTOUR_THREAD;

import java.util.StringTokenizer;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
///-import edu.buffalo.cse.jive.model.IPathModel.IContextPath;
///-import edu.buffalo.cse.jive.model.IPathModel.IPathMember;
///-import edu.buffalo.cse.jive.model.IPathModel.IMethodPath;
import edu.buffalo.cse.jive.model.IModel.IOutOfModelMethodReference;
import edu.buffalo.cse.jive.model.IModel.IThreadValue;
import edu.buffalo.cse.jive.model.IModel.IValue;
import edu.buffalo.cse.jive.model.IStaticModel.NodeKind;
import edu.buffalo.cse.jive.model.IStaticModel.NodeModifier;
import edu.buffalo.cse.jive.ui.IPathAttributes;
import edu.buffalo.cse.jive.ui.IMemberAttributes;

public class PathAttributesFactory
{
  public static IPathAttributes createInstanceAttributes(final IPathStartEvent path)
  {
    return new InstancePathAttributes(path);
  }

  public static IPathAttributes createStaticAttributes(final IPathStartEvent path)
  {
    return new StaticPathAttributes(path);
  }

  public static IPathAttributes createThreadAttributes(final IThreadValue thread,
      final Color backgroundColor)
  {
    return new ThreadAttributes(thread, backgroundColor);
  }

  private abstract static class PathAttributes implements IPathAttributes
  {
    private static char VALUE_TEXT_DELIMITER = '.';
    private static char VALUE_TYPE_DELIMITER = '$';
    private static char VALUE_METHOD_DELIMITER = '#';

    protected String computeDefaultText(final String text)
    {
    	return text;
    }

    protected String computeMethodText(final String text)
    {
      return text.substring(text.lastIndexOf(PathAttributes.VALUE_METHOD_DELIMITER) + 1);
    }
  }

  private static class InstancePathAttributes extends PathAttributes
  {
    private static final Image CONTOUR_IMAGE = IM_OM_CONTOUR_INSTANCE.enabledImage();
    private final String text;
    private final String toolTipText;

    private InstancePathAttributes(final IPathStartEvent path)
    {
      toolTipText = ""+path.nextChoice()+"";
      text=""+path.eventId()+"";
    }

    @Override
    public Image getIcon()
    {
      return InstancePathAttributes.CONTOUR_IMAGE;
    }

    @Override
    public Color getLabelBackgroundColor()
    {
      return IPathAttributes.BACKGROUND_COLOR_OBJECT;
    }

    @Override
    public String getText()
    {
      return text;
    }

    @Override
    public Image getToolTipIcon()
    {
      return InstancePathAttributes.CONTOUR_IMAGE;
    }

    @Override
    public String getToolTipText()
    {
      return toolTipText;
    }
  }


  private static class StaticPathAttributes extends PathAttributes
 {
		private static final Image CLASS_IMAGE = IM_OM_CONTOUR_STATIC
				.enabledImage();
		private static final Image INTERFACE_IMAGE = IM_OM_CONTOUR_INTERFACE
				.enabledImage();
		private boolean isInterface;
		private String text;
		private String toolTipText;

		// gets displayed
		private StaticPathAttributes(IPathStartEvent path) {
			try {
				isInterface = false;
				String ttt, tt = null;

				if (path.varToBeChanged().contains("ERROR:")) {
					ttt = path.varToBeChanged() + "\n" + "Instruction="
							+ path.instructionCG();
					tt = path.varToBeChanged();
				} else if (path.varToBeChanged().equalsIgnoreCase("th")) {
					StringTokenizer strTkn = new StringTokenizer(
							path.nextChoice(), "|");
					tt = strTkn.nextToken();
					ttt = "State=" + strTkn.nextToken() + "\n" + "Instruction="
							+ path.instructionCG();

				} else {
					// ttt =
					// "Choice = "+path.nextChoice()+"\nEvent id = "+path.eventId()+"\nParent id ="+path.getParentEvent().eventId()+"\nVar ="+path.varToBeChanged()+"\nthisid = "+path.thisId()+"\n"+"Instruction="+path.instructionCG();
					ttt = "Instruction=" + path.instructionCG();
					tt = path.varToBeChanged() + "=" + path.nextChoice();
				}

				toolTipText = ttt;
				text = tt;
			} catch (Exception e) {
				isInterface = true;
				text = "";
				toolTipText = "error";
				e.printStackTrace();
			}
		}

		@Override
		public Image getIcon() {
			return isInterface ? StaticPathAttributes.INTERFACE_IMAGE
					: StaticPathAttributes.CLASS_IMAGE;
		}

		@Override
		public Color getLabelBackgroundColor() {
			return isInterface ? IPathAttributes.BACKGROUND_COLOR_INTERFACE
					: IPathAttributes.BACKGROUND_COLOR_CLASS;
		}

		@Override
		public String getText() {
			return text;
		}

		@Override
		public Image getToolTipIcon() {
			return isInterface ? StaticPathAttributes.INTERFACE_IMAGE
					: StaticPathAttributes.CLASS_IMAGE;
		}

		@Override
		public String getToolTipText() {
			return toolTipText;
		}
	}

  private static final class ThreadAttributes extends PathAttributes
  {
    private final Color backgroundColor;
    private final IThreadValue thread;

    private ThreadAttributes(final IThreadValue thread, final Color backgroundColor)
    {
      this.backgroundColor = backgroundColor;
      this.thread = thread;
    }

    @Override
    public Image getIcon()
    {
      return IM_OM_CONTOUR_THREAD.enabledImage();
    }

    @Override
    public Color getLabelBackgroundColor()
    {
      return backgroundColor;
      // return IPathAttributes.BACKGROUND_COLOR_LIGHT_ORANGE;
    }

    @Override
    public String getText()
    {
      return thread.toString();
    }

    @Override
    public Image getToolTipIcon()
    {
      return IM_OM_CONTOUR_THREAD.enabledImage();
    }

    @Override
    public String getToolTipText()
    {
      return thread.toString();
    }
  }
}