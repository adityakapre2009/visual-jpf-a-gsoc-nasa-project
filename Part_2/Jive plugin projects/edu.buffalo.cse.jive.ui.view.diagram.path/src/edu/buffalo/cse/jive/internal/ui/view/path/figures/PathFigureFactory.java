package edu.buffalo.cse.jive.internal.ui.view.path.figures;

import org.eclipse.draw2d.IFigure;

public final class PathFigureFactory
{
  public static IFigure createHierarchicalPathFigure()
  {
    return new HierarchicalPathDiagramFigure();
  }

  public static IFigure createTabularPathFigure()
  {
    return new TabularPathDiagramFigure();
  }
}
