package edu.buffalo.cse.jive.internal.ui.view.path.editparts;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.swt.graphics.Color;

import edu.buffalo.cse.jive.debug.model.IJiveDebugTarget;
import edu.buffalo.cse.jive.internal.ui.PathAttributesFactory;
import edu.buffalo.cse.jive.internal.ui.view.path.figures.PathFigure;
///-import edu.buffalo.cse.jive.model.IPathModel.IContextPath;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
///-import edu.buffalo.cse.jive.model.IPathModel.IPath;
///-import edu.buffalo.cse.jive.model.IPathModel.IPathMember;
///-import edu.buffalo.cse.jive.model.IPathModel.IMethodPath;
import edu.buffalo.cse.jive.model.IExecutionModel;
import edu.buffalo.cse.jive.model.IStaticModel.NodeKind;
import edu.buffalo.cse.jive.ui.IPathAttributes;
import edu.buffalo.cse.jive.ui.IThreadColorManager;
import edu.buffalo.cse.jive.ui.JiveUIPlugin;
import edu.buffalo.cse.jive.ui.VisualStatus;

/**
 * An interface representing the visual state of a path.
 * 
 * @see PathEditPart
 */
abstract class PathState
 {
	static PathState createMinimizedState(final PathEditPart editPart) {
		// System.out.println("PathState :: createMinimizedState(final PathEditPart editPart)");
		return new PathStateMinimized(editPart);
	}

	static PathState createObjectState(final PathEditPart editPart) {
		// System.out.println("PathState :: createObjectState(final PathEditPart editPart)");
		return new PathStateObject(editPart);
	}

	static PathState createStackedState(final PathEditPart editPart) {
		// System.out.println("PathState :: createStackedState(final PathEditPart editPart)");
		return new PathStateStacked(editPart);
	}

	private final PathEditPart editPart;

	private PathState(final PathEditPart editPart) {
		this.editPart = editPart;
	}

	protected IPathAttributes pathFigureAttributes(final IPathStartEvent path) {
		// System.out.println("PathState :: pathFigureAttributes(final IPathStartEvent path)");
		try {
			return PathAttributesFactory.createStaticAttributes(path);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	protected PathFigure createFullBuilder(final IPathStartEvent path) {
		// System.out.println("PathState :: createFullBuilder(final IPathStartEvent path)");
		final IPathAttributes attributes = pathFigureAttributes(path);
		final PathFigure figure = new PathFigure(VisualStatus.FULL, attributes);
		return figure;
	}

	protected IExecutionModel executionModel() {
		return editPart.executionModel();
	}

	protected void exposeMembers(final IPathStartEvent path,
			final PathFigure figure) {

	}

	protected boolean focusCallPath() {
		return editPart.focusCallPath();
	}

	protected PathEditPart getEditPart() {
		return editPart;
	}

	// Recursive method to determine if path contains a method
	protected boolean hasMethod(final IPathStartEvent path) {
		return false;
	}

	protected boolean showMemberTables() {
		return editPart.showMemberTables();
	}

	abstract IFigure createFigure();

	IFigure getFigure() {
		return editPart.getFigure();
	}

	Object getModel() {
		return editPart.getModel();
	}

	ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	void refreshVisuals() {

	}

	/**
	 * A class representing an object path. It displays the path as a labeled
	 * dot.
	 * 
	 * @see PathState
	 * @see PathEditPart
	 */
	private static class PathStateMinimized extends PathState {
		private PathStateMinimized(final PathEditPart editPart) {
			super(editPart);
		}

		private VisualStatus pathFigureState(final IPathStartEvent path) {
			return null;
		}

		@Override
		IFigure createFigure() {
			final IPathStartEvent path = (IPathStartEvent) getModel();
			final VisualStatus figureStatus = pathFigureState(path);
			if (figureStatus == VisualStatus.EMPTY) {
				return new PathFigure(figureStatus, null);
			} else {
				final IExecutionModel model = executionModel();
				model.readLock();
				try {
					final IPathAttributes attributes = pathFigureAttributes(path);
					final PathFigure figure = new PathFigure(figureStatus,
							attributes);
					return figure;
				} finally {
					model.readUnlock();
				}
			}
		}
	}

	/**
	 * A class representing an object path. It supports showing member tables
	 * and focusing on the call path. Hence, this class encapsulates four
	 * different states.
	 * 
	 * @see PathState
	 * @see PathEditPart
	 */
	private static class PathStateObject extends PathState {
		private PathStateObject(final PathEditPart editPart) {
			super(editPart);
		}

		@Override
		IFigure createFigure() {
			final IPathStartEvent path = (IPathStartEvent) getModel();
			final IExecutionModel model = executionModel();
			model.readLock();
			try {
				boolean renderFull = false;
				final PathFigure figure;
				// no focusing on the call path-- build a full path
				if (!focusCallPath()) {
					figure = createFullBuilder(path);
				} else {
					// when focusing on the call path we must determine how much
					// to draw
					final IPathAttributes attributes = pathFigureAttributes(path);
					// we render full paths if the path is static or has some
					// pending method call
					// show the innermost path only
					if (path.children().size() == 0) {
						figure = new PathFigure(VisualStatus.NODE, attributes);
					}
					// when minimized, do not show paths for containing types
					else {
						figure = new PathFigure(VisualStatus.EMPTY, attributes);
					}

				}
				// we only show member tables for full renderings
				if (showMemberTables() && (renderFull || !focusCallPath())) {
					exposeMembers(path, figure);
				}
				return figure;
			} finally {
				model.readUnlock();
			}
		}
	}

	/**
	 * A class representing a stacked path. It supports showing member tables
	 * and focusing on the call path. Hence, this class encapsulates four
	 * different states.
	 * 
	 * @see PathState
	 * @see PathEditPart
	 */
	private static class PathStateStacked extends PathState {
		private PathStateStacked(final PathEditPart editPart) {
			super(editPart);
		}

		private IFigure createBuilder(final IPathStartEvent path) {
			boolean renderFull = false;
			final PathFigure figure;
			// no focusing on the call path-- build a full path
			if (!focusCallPath()) {
				figure = createFullBuilder(path);
			} else {
				// when focusing on the call path we must determine how much to
				// draw
				final IPathAttributes attributes = pathFigureAttributes(path);

				{
					figure = new PathFigure(VisualStatus.FULL, attributes);
				}
			}
			// we only show member tables for full renderings
			if (showMemberTables() && (renderFull || !focusCallPath())) {
				exposeMembers(path, figure);
			}
			return figure;
		}

		private IFigure figure(final IPathStartEvent path) {
			if (path instanceof IPathStartEvent) {
				final IPathStartEvent cc = (IPathStartEvent) path;
				final PathFigure figure = createFullBuilder(path);
				// we show member tables for static paths
				if (showMemberTables()) {
					exposeMembers(path, figure);
				}
				return createBuilder(path);
			}
			return null;
		}

		@Override
		IFigure createFigure() {
			final IPathStartEvent path = (IPathStartEvent) getModel();
			final IExecutionModel model = executionModel();
			model.readLock();
			try {
				return figure(path);
			} finally {
				model.readUnlock();
			}
		}
	}
}
