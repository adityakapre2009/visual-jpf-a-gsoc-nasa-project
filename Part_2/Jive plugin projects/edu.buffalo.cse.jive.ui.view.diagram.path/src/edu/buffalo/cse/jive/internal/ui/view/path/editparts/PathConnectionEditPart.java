package edu.buffalo.cse.jive.internal.ui.view.path.editparts;

import org.eclipse.draw2d.Label;
import java.util.Iterator;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionEndpointLocator;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MidpointLocator;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;

import edu.buffalo.cse.jive.debug.model.IJiveDebugTarget;
import edu.buffalo.cse.jive.internal.ui.view.path.graph.PathGraph;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
///-import edu.buffalo.cse.jive.model.IPathModel.IPathMember;
///-import edu.buffalo.cse.jive.model.IPathModel.IMethodPath;
///-import edu.buffalo.cse.jive.model.IModel.IPathReference;
import edu.buffalo.cse.jive.model.IModel.IOutOfModelMethodReference;
import edu.buffalo.cse.jive.model.IModel.IValue;
import edu.buffalo.cse.jive.model.IStaticModel.NodeKind;
import edu.buffalo.cse.jive.model.IStaticModel.NodeModifier;
import edu.buffalo.cse.jive.ui.IJiveEditPart;
import edu.buffalo.cse.jive.ui.IThreadColorManager;
import edu.buffalo.cse.jive.ui.JiveUIPlugin;
import edu.buffalo.cse.jive.ui.view.diagram.path.PathDiagramView;

class LabeledConnectionFigure extends PolylineConnection {
	protected Label label;

	public LabeledConnectionFigure(String lb) {
		setTargetDecoration(new PolygonDecoration());
		MidpointLocator labelLocator = new MidpointLocator(this, 0);
		label = new Label(lb);
		label.setOpaque(true);
		add(label, labelLocator);
	}

	public void setLabelText(String labelText) {
		label.setText(labelText);
	}

	public String getLabelText() {
		return label.getText();
	}
}

public class PathConnectionEditPart extends AbstractConnectionEditPart implements IJiveEditPart
 {
	String lb;

	public PathConnectionEditPart(final Object model, String lb) {
		super();
		setModel(model);
		this.lb = lb;
	}

	@Override
	protected void createEditPolicies() {
		// TODO Determine if anything should be done here
	}

	@Override
	protected IFigure createFigure() {
		try {
			final LabeledConnectionFigure connection = new LabeledConnectionFigure(
					this.lb);
			connection.setLineStyle(SWT.LINE_SOLID);
			connection.setLineWidth(1);
			connection.setTargetDecoration(new PolygonDecoration());
			final Long instance = (Long) getModel();
			final PathDiagramEditPart contents = (PathDiagramEditPart) getRoot()
					.getContents();
			final IJiveDebugTarget target = contents.getModel();
			connection.setLineWidth(2);
			connection.setLineWidth(2);
			connection.setForegroundColor(ColorConstants.gray);
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
