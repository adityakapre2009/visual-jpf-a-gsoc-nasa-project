package edu.buffalo.cse.jive.internal.ui.view.path.figures;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.CompoundBorder;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import edu.buffalo.cse.jive.internal.ui.CustomLineBorder;
import edu.buffalo.cse.jive.ui.IPathAttributes;
import edu.buffalo.cse.jive.ui.VisualStatus;

/**
 * A Draw2d {@code Figure} used to visualize a {@code Path} from a {@code PathModel}.
 * 
 * @see edu.buffalo.cse.jive.model.IPath
 */
public class PathFigure extends Figure
{
  /**
   * The border used around the path's child compartment.
   */
  protected static final Border CHILD_COMPARTMENT_BORDER = new MarginBorder(4);
  /**
   * The background color for the path.
   */
  protected static final Color CONTOUR_BACKGROUND_COLOR = new Color(null, 255, 255, 236);
  /**
   * The border used for the overall path figure.
   */
  protected static final Border CONTOUR_BORDER = new LineBorder(1);
  /**
   * The border used around the path's label.
   */
  protected static final Border LABEL_BORDER = new CompoundBorder(new CustomLineBorder(0, 0, 1, 0),
      new MarginBorder(1, 2, 1, 6));
  /**
   * The figure containing visualizations of the path's children.
   */
  private Figure childCompartment;
  /**
   * The path's label, which typically represents the path ID.
   */
  private Label label;
  /**
   * The figure which visualizes the path's members.
   */
  private PathMemberTableFigure memberTable;

  private IFigure contentPane;
  
  // TODO Remove this and JavaPathFigure (and subclasses)
  public PathFigure()
  {
	  
  }
  
  public IFigure getContentPane(){
		return contentPane;
	}

  /**
   * Constructs the path figure.
   */
  public PathFigure(final VisualStatus state, final IPathAttributes attributes)
  {
    super();
    switch (state)
    {
      case FULL:
        initializeFull(attributes);
        return;
      case NODE:
        initializeNode(attributes);
        return;
      case OUTLINE:
        initializeOutline(attributes);
        return;
      case EMPTY:
        initializeEmpty(attributes);
        return;
    }
    throw new IllegalStateException("State " + state + " is not implemented.");
  }

  @Override
  public void add(final IFigure figure, final Object constraint, final int index)
  {
    if (figure == label || figure == memberTable || figure == childCompartment)
    {
      super.add(figure, constraint, index);
    }
    else
    {
      childCompartment.add(figure, constraint, index);
    }
  }

  public PathMemberTableFigure getMemberTable()
  {
    return memberTable;
  }

  @Override
  public void paint(final Graphics graphics)
  {
    graphics.setAntialias(SWT.ON);
    graphics.setTextAntialias(SWT.ON);
    super.paint(graphics);
    graphics.setAntialias(SWT.OFF);
    graphics.setTextAntialias(SWT.OFF);
  }

  @Override
  public void remove(final IFigure figure)
  {
    if (figure == label || figure == memberTable || figure == childCompartment)
    {
      super.remove(figure);
    }
    else
    {
      childCompartment.remove(figure);
    }
  }

  private void initializeEmpty(final IPathAttributes attributes)
  {
    final FlowLayout layout = new FlowLayout();
    setLayoutManager(layout);
    initializeCompartment();
    childCompartment.setBorder(null);
    add(childCompartment);
  }

  private void initializeFull(final IPathAttributes attributes)
  {
	//System.out.println("PathFigure :: initializeFull(final IPathAttributes attributes)");
    initializePath();
    initializeLabel(attributes);
    initializeMemberTable();
    initializeCompartment();
    add(label);
    add(memberTable);
    add(childCompartment);
  }

  private void initializeNode(final IPathAttributes attributes)
  {
    final FlowLayout layout = new FlowLayout(false);
    setLayoutManager(layout);
    initializeLabel(attributes);
    label.setBorder(null);
    label.setBackgroundColor(null);
    initializeCompartment();
    childCompartment.setBorder(null);
    add(label);
    add(childCompartment);
  }

  private void initializeOutline(final IPathAttributes attributes)
  {
    final FlowLayout layout = new FlowLayout();
    setLayoutManager(layout);
    setBorder(PathFigure.CONTOUR_BORDER);
    setBackgroundColor(PathFigure.CONTOUR_BACKGROUND_COLOR);
    setOpaque(true);
    initializeCompartment();
    add(childCompartment);
  }

  /**
   * Initializes the path figure's child compartment. This method is called from
   * {@link #initialize()}.
   */
  protected void initializeCompartment()
  {
    final ToolbarLayout layout = new ToolbarLayout(true); // TODO determine if we want horizontal or
                                                          // vertical arrangement
    layout.setSpacing(4);
    childCompartment = new Figure();
    childCompartment.setLayoutManager(layout);
    childCompartment.setOpaque(true);
    childCompartment.setBorder(PathFigure.CHILD_COMPARTMENT_BORDER);
  }

  /**
   * Initializes the overall path figure. This method is called from {@link #initialize()}.
   */
  protected void initializePath()
  {
    final ToolbarLayout layout = new ToolbarLayout(false);
    layout.setStretchMinorAxis(true);
    setLayoutManager(layout);
    setOpaque(true);
    setBorder(PathFigure.CONTOUR_BORDER);
    setBackgroundColor(PathFigure.CONTOUR_BACKGROUND_COLOR);
  }

  /**
   * Initializes the path figure's label. This method is called from {@link #initialize()}.
   */
  protected void initializeLabel(final IPathAttributes attributes)
  {
    label = new Label(attributes.getText(), attributes.getIcon());
    label.setOpaque(true);
    label.setToolTip(new Label(attributes.getToolTipText(), attributes.getToolTipIcon()));
    label.setBorder(PathFigure.LABEL_BORDER);
    label.setBackgroundColor(attributes.getLabelBackgroundColor());
    label.setIconAlignment(PositionConstants.BOTTOM);
    label.setLabelAlignment(PositionConstants.LEFT);
    
  }

  /**
   * Initializes the path figure's member table. This method is called from {@link #initialize()}
   * .
   */
  protected void initializeMemberTable()
  {
    memberTable = new PathMemberTableFigure();
  }

  /**
   * A Draw2d figure used to visualize {@code JavaPath}s.
   * 
   * @see edu.buffalo.cse.jive.model.IPath.StaticPath
   */
  public static abstract class LabeledPathFigure extends PathFigure
  {
    /**
     * The string used for the path figure's label.
     */
    private final String labelText;
    /**
     * The string used for the tool tip of the path figure's label.
     */
    private final String toolTipText;

    /**
     * Constructs the path figure with the supplied identifier to be used by the path figure's
     * label. The last portion of the identifier is used for the label, and the complete identifier
     * is used for the label's tool tip. The last portion is defined by all text after the last
     * occurrence of the delimiter returned by {@link #getPathTextDelimiter()}.
     * 
     * @param id
     *          the string for the path figure's label
     */
    protected LabeledPathFigure(final String id)
    {
      super();
      final int index = id.lastIndexOf(getPathTextDelimiter());
      labelText = id.substring(index + 1);
      toolTipText = id;
    }

    /**
     * Returns the image used for the path figure's label.
     * 
     * @return the image for the figure's label
     */
    protected abstract Image getPathImage();

    /**
     * Returns the delimiter to be used when determining the path figure's label.
     * 
     * @return the delimiter for the path figure's label
     * @see #JavaPathFigure(String)
     */
    protected abstract char getPathTextDelimiter();

    protected Image getLabelIcon()
    {
      return getPathImage();
    }

    protected String getLabelText()
    {
      return labelText;
    }

    protected Image getToolTipIcon()
    {
      return getPathImage();
    }

    protected String getToolTipText()
    {
      return toolTipText;
    }
  }
}