package edu.buffalo.cse.jive.internal.ui.view.path.editparts;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editparts.AbstractTreeEditPart;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.widgets.Display;

import edu.buffalo.cse.jive.debug.model.IJiveDebugTarget;
import edu.buffalo.cse.jive.internal.ui.view.path.graph.PathGraph;
//import edu.buffalo.cse.jive.model.IPathModel.IPath;
import edu.buffalo.cse.jive.model.IEventModel.IJiveEvent;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
import edu.buffalo.cse.jive.model.IExecutionModel;
import edu.buffalo.cse.jive.model.IExecutionModel.ITraceViewListener;
import edu.buffalo.cse.jive.preferences.PreferenceKeys;
import edu.buffalo.cse.jive.preferences.PreferencesPlugin;
import edu.buffalo.cse.jive.ui.IJiveDiagramEditPart;
import edu.buffalo.cse.jive.ui.IStepAction;
import edu.buffalo.cse.jive.ui.IStepListener;
import edu.buffalo.cse.jive.ui.IStepManager;
import edu.buffalo.cse.jive.ui.IThreadColorListener;
import edu.buffalo.cse.jive.ui.IThreadColorManager;
import edu.buffalo.cse.jive.ui.JiveUIPlugin;
import edu.buffalo.cse.jive.ui.view.diagram.path.PathDiagramFactory;

/**
 * An {@code EditPart} serving as a controller for the state part of the execution model, which is
 * visualized by a {@code PathDiagramFigure}. The edit part also serves as a path model
 * listener. It handles path model events by delegating to the appropriate edit part.
 * 
 * @see PathEditPart
 * @see PathConnectionEditPart
 * @see TabularPathDiagramFigure
 */
public class PathDiagramEditPart extends AbstractGraphicalEditPart 
implements IJiveDiagramEditPart, ITraceViewListener, IDebugEventSetListener, IPropertyChangeListener,
    IThreadColorListener, IStepListener
 {
	private boolean callPathFocus;
	private State pathState;
	private PathGraph graph;
	private volatile boolean modelChanged = false;
	private static int ii = 0;
	/**
	 * scrollLock was being used only to reveal the last method path; since this
	 * was not documented and I found no obvious case in which the call to
	 * reveal last method path made any difference, I removed it from the code;
	 * I never understood the relationship between reveal last method path and
	 * scroll lock...
	 */
	@SuppressWarnings("unused")
	private boolean scrollLock;
	private long updateInterval;
	private final Job updateJob = new Job("OD Update Job") {

		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			try {
				final IJiveDebugTarget target = getModel();
				if (modelChanged && target.viewsEnabled()) {
					// System.out.println("- Going to refresh modelChanged && target.viewsEnabled()......");
					update();
					modelChanged = false;
				}
				return Status.OK_STATUS;
			} catch (final Exception e) {
				JiveUIPlugin.log(e);
				return Status.OK_STATUS;
			} finally {
				schedule(updateInterval);
			}
		}

	};

	public PathDiagramEditPart(final Object model) {
		try {
			final IPreferenceStore store = PreferencesPlugin.getDefault()
					.getPreferenceStore();
			updatePathState(store.getString(PreferenceKeys.PREF_OD_STATE));
			updateCallPathFocus(store
					.getBoolean(PreferenceKeys.PREF_OD_CALLPATH_FOCUS));
			updateScrollLock(store.getBoolean(PreferenceKeys.PREF_SCROLL_LOCK));
			updateUpdateInterval(store
					.getLong(PreferenceKeys.PREF_UPDATE_INTERVAL));
			setModel(model);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void activate() {
		try {
			super.activate();
			final IExecutionModel model = executionModel();
			// System.out.println("- in activate");
			model.traceView().register(this);
			DebugPlugin.getDefault().addDebugEventListener(this);
			final IPreferenceStore store = PreferencesPlugin.getDefault()
					.getPreferenceStore();
			store.addPropertyChangeListener(this);
			final IThreadColorManager colorManager = JiveUIPlugin.getDefault()
					.getThreadColorManager();
			colorManager.addThreadColorListener(this);
			final IStepManager stepManager = JiveUIPlugin.getDefault()
					.stepManager();
			stepManager.addStepListener(this);
			updateJob.setSystem(true);
			updateJob.schedule();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deactivate() {
		try {
			final IExecutionModel model = executionModel();
			// System.out.println("- in deactivate");
			if (model != null) {
				model.traceView().unregister(this);
			}
			DebugPlugin.getDefault().removeDebugEventListener(this);
			final IPreferenceStore store = PreferencesPlugin.getDefault()
					.getPreferenceStore();
			store.removePropertyChangeListener(this);
			final IThreadColorManager manager = JiveUIPlugin.getDefault()
					.getThreadColorManager();
			manager.removeThreadColorListener(this);
			final IStepManager stepManager = JiveUIPlugin.getDefault()
					.stepManager();
			stepManager.removeStepListener(this);
			updateJob.cancel();
			setSelected(EditPart.SELECTED_NONE); // TODO Determine if this is
													// needed
			super.deactivate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eventsInserted(final List<IJiveEvent> events) {
		// System.out.println("PathDiagramEditPart :: eventsInserted(final List<IJiveEvent> events)");
		modelChanged = true;
	}

	public IExecutionModel executionModel() {
		try {
			// System.out.println("PathDiagramEditPart :: executionModel()");
			final IJiveDebugTarget target = getModel();
			// System.out.println("- target " + target);
			if (target != null) {
				// System.out.println("- model " + target.model());
			}
			return target != null ? target.model() : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public State getPathState() {
		return pathState;
	}

	@Override
	public IJiveDebugTarget getModel() {
		try {
			return (IJiveDebugTarget) super.getModel();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void handleDebugEvents(final DebugEvent[] events) {
		modelChanged = true;
	}

	public boolean isCallPathFocused() {
		return callPathFocus;
	}

	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		try {
			final String property = event.getProperty();
			if (property.equals(PreferenceKeys.PREF_OD_STATE)) {
				updatePathState((String) event.getNewValue());
				forceUpdate();
			} else if (property.equals(PreferenceKeys.PREF_OD_CALLPATH_FOCUS)) {
				updateCallPathFocus((Boolean) event.getNewValue());
				forceUpdate();
			} else if (property.equals(PreferenceKeys.PREF_SCROLL_LOCK)) {
				updateScrollLock((Boolean) event.getNewValue());
				forceUpdate();
			} else if (property.equals(PreferenceKeys.PREF_UPDATE_INTERVAL)) {
				final IPreferenceStore store = PreferencesPlugin.getDefault()
						.getPreferenceStore();
				updateUpdateInterval(store
						.getLong(PreferenceKeys.PREF_UPDATE_INTERVAL));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			// System.out.println("PathDiagramEditPart :: refresh()");
			final IJiveDebugTarget target = getModel();
			if (!target.viewsEnabled()) {
				return;
			}
			executionModel().readLock();
			try {

				graph = new PathGraph(executionModel());
				// refreshChildren gets called from {@code super.refresh()} with
				// an up-to-date graph
				super.refresh();
			} finally {
				executionModel().readUnlock();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void steppingCompleted(final IJiveDebugTarget target,
			final IStepAction action) {
		try {
			if (getModel() == target) {
				forceUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void steppingInitiated(final IJiveDebugTarget target) {
		// no-op
	}

	@Override
	public void threadColorsChanged(final IJiveDebugTarget target) {
		if (getModel() == target) {
			forceUpdate();
		}
	}

	@Override
	public void traceVirtualized(final boolean isVirtual) {
		forceUpdate();
	}

	private void forceUpdate() {
		update();
		modelChanged = false;
	}

	private void update() {
		try {
			// System.out.println("PathDiagramEditPart : update()");
			final Display display = JiveUIPlugin.getStandardDisplay();
			display.asyncExec(new Runnable() {
				@Override
				public void run() {
					if (isActive()) {
						for (final Object o : getChildren().toArray()) {
							removeChild((EditPart) o);
						}
						refresh();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateCallPathFocus(final boolean callPathFocus) {
		this.callPathFocus = callPathFocus;
	}

	private void updatePathState(final String state) {
		if (state.equals(PreferenceKeys.PREF_OD_OBJECTS)) {
			pathState = State.OBJECTS;
		} else if (state.equals(PreferenceKeys.PREF_OD_OBJECTS_MEMBERS)) {
			pathState = State.OBJECTS_MEMBERS;
		} else if (state.equals(PreferenceKeys.PREF_OD_STACKED)) {
			pathState = State.STACKED;
		} else if (state.equals(PreferenceKeys.PREF_OD_STACKED_MEMBERS)) {
			pathState = State.STACKED_MEMBERS;
		} else if (state.equals(PreferenceKeys.PREF_OD_MINIMIZED)) {
			pathState = State.MINIMIZED;
		} else {
			pathState = State.STACKED;
		}
	}

	private void updateScrollLock(final boolean scrollLock) {
		this.scrollLock = scrollLock;
	}

	private void updateUpdateInterval(final long interval) {

		updateInterval = interval;
	}

	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		try {

			assert graph != null : "The path graph was not constructed.";
			final IPathStartEvent c = (IPathStartEvent) childEditPart
					.getModel();
			final IFigure childFigure = ((GraphicalEditPart) childEditPart)
					.getFigure();

			ii++;
			getFigure().add(childFigure, graph.getPosition(c), ii);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void createEditPolicies() {
	}

	@Override
	protected IFigure createFigure() {
		return PathDiagramFactory.createPathDigramFigure();
	}

	@Override
	protected List<IPathStartEvent> getModelChildren() {
		try {
			// System.out.println("PathDiagramEditPart :: getModelChildren()");
			return executionModel().pathView().getAllChildren();
		} catch (Exception e) {
			if (e instanceof NullPointerException) {
				// System.out.println("- no children");
			}

			return null;
		}
	}

	public enum State {
		MINIMIZED, OBJECTS, OBJECTS_MEMBERS, STACKED, STACKED_MEMBERS
	}
}
