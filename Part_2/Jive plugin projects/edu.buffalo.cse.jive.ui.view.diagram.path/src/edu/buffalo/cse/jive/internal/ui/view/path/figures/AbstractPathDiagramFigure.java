package edu.buffalo.cse.jive.internal.ui.view.path.figures;

import java.util.Map;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.ToolbarLayout;

import edu.buffalo.cse.jive.internal.ui.view.path.graph.INodePosition;
import edu.buffalo.cse.jive.model.lib.Tools;

abstract class AbstractPathDiagramFigure extends Figure
{
  /**
   * The border used around the path diagram.
   */
  private static final Border CLIENT_AREA_BORDER = new MarginBorder(8);
  private final Map<INodePosition, IFigure> positionToFigure;
  private final IFigure reachableSection;
  private final IFigure staticSection;
  private final IFigure unreachableSection;
  private final IFigure singletonsSection;

  AbstractPathDiagramFigure()
  {
    super();
    staticSection = new Figure();
    final ToolbarLayout staticSectionLayout = new ToolbarLayout(true);
    staticSectionLayout.setSpacing(30);
    staticSection.setLayoutManager(staticSectionLayout);
    reachableSection = new Figure();
    final ToolbarLayout reachableSectionLayout = new ToolbarLayout(true);
    reachableSectionLayout.setSpacing(30);
    reachableSection.setLayoutManager(reachableSectionLayout);
    unreachableSection = new Figure();
    final ToolbarLayout unreachableSectionLayout = new ToolbarLayout(true);
    unreachableSectionLayout.setSpacing(30);
    unreachableSection.setLayoutManager(unreachableSectionLayout);
    singletonsSection = new Figure();
    final ToolbarLayout singletonsSectionLayout = new ToolbarLayout(true);
    singletonsSectionLayout.setSpacing(30);
    singletonsSection.setLayoutManager(singletonsSectionLayout);
    positionToFigure = Tools.newHashMap();
    final ToolbarLayout layout = new ToolbarLayout(false);
    layout.setSpacing(30);
    setLayoutManager(layout);
    setBorder(AbstractPathDiagramFigure.CLIENT_AREA_BORDER);
    add(staticSection);
    add(reachableSection);
    add(unreachableSection);
    add(singletonsSection);
  }

  @Override
  public void add(final IFigure f, final Object constraint, final int index)
  {
    if (f instanceof PathFigure)
    {
      assert constraint instanceof INodePosition;
      final PathFigure figure = (PathFigure) f;
      final INodePosition position = (INodePosition) constraint;
      switch (position.section())
      {
        case DS_STATIC:
          addStaticFigure(figure, position);
          break;
        case DS_REACHABLE:
          addNonStaticFigure(figure, position);
          break;
    ///-    case DS_UNREACHABLE:
    ///-      addNonStaticFigure(figure, position);
    ///-      break;
    ///-    case DS_SINGLETONS:
    ///-      addNonStaticFigure(figure, position);
    ///-      break;
      }
    }
    else
    {
      super.add(f, constraint, index);
    }
  }

  @Override
  public void remove(final IFigure figure)
  {
    if (figure instanceof PathFigure)
    {
      removeFigure((PathFigure) figure);
    }
    else
    {
      super.remove(figure);
    }
  }

  protected abstract void addNonStaticFigure(final PathFigure figure,
      final INodePosition position);

  protected void addStaticFigure(final PathFigure figure, final INodePosition position)
  {
	//System.out.println("AbstractPathDiagramFigure :: addStaticFigure(final PathFigure figure, final INodePosition position)");
    sectionStatic().add(figure);
  }

  protected Map<INodePosition, IFigure> positionToFigure()
  {
    return positionToFigure;
  }

  protected abstract void removeFigure(final PathFigure figure);

  protected IFigure sectionReachable()
  {
    return reachableSection;
  }

  protected IFigure sectionSingletons()
  {
    return singletonsSection;
  }

  protected IFigure sectionStatic()
  {
    return staticSection;
  }

  protected IFigure sectionUnreachable()
  {
    return unreachableSection;
  }
}
