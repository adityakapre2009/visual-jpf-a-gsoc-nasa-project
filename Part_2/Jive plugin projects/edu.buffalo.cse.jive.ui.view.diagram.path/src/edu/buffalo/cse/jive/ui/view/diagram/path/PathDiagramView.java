package edu.buffalo.cse.jive.ui.view.diagram.path;
import static edu.buffalo.cse.jive.preferences.ImageInfo.IM_BASE_TREE;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;

import edu.buffalo.cse.jive.debug.model.IJiveDebugTarget;
import edu.buffalo.cse.jive.internal.ui.view.path.actions.PathStateActionFactory;
import edu.buffalo.cse.jive.internal.ui.view.path.editparts.PathConnectionEditPart;
import edu.buffalo.cse.jive.internal.ui.view.path.editparts.PathEditPart;
import edu.buffalo.cse.jive.internal.ui.view.path.editparts.PathDiagramEditPart;
import edu.buffalo.cse.jive.internal.ui.view.path.graph.PathGraph;
///-import edu.buffalo.cse.jive.model.IPathModel.IPath;
///-import edu.buffalo.cse.jive.model.IPathModel.IPathMember;
import edu.buffalo.cse.jive.model.IEventModel.IJiveEvent;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
import edu.buffalo.cse.jive.ui.view.AbstractGraphicalJiveView;

/**
 * A view part to present {@code InteractivePathModel}s associated with {@code IJiveDebugTarget}
 * s. The content provider used by this view is specific to Java path models. The view uses a GEF
 * {@code ScrollingGraphicalViewer} to display the paths. Controls are also available to step and
 * run through model transactions in both the forward and reverse directions.
 * 
 * @see IJiveDebugTarget
 * @see ScrollingGraphicalViewer
 * @see PathModelEditPartFactory
 */
public class PathDiagramView extends AbstractGraphicalJiveView
{
  /**
   * The group used to actions specific to the path diagram.
   */
  private static final String PATH_DIAGRAM_GROUP = "pathDiagramGroup";
  private IAction fExpandedObjectStateAction;
  private IAction fExpandedStackedStateAction;
  private IAction fMinimizedStateAction;
  private IAction fObjectCallPathStateAction;
  private IAction fObjectStateAction;
  private IAction fScrollLockAction;
  private IAction fStackedStateAction;

  
  /**
   * Constructs the view.
   */
  public PathDiagramView()
  {
    super();
  }

  @Override
  public void dispose()
  {
    super.dispose();
  }

  @Override
  protected void configurePullDownMenu(final IMenuManager manager)
  {
    // org.eclipse.gef.ui.actions.GEFActionConstants
    manager.add(fObjectStateAction); // #1. "Objects" (default)
    manager.add(fStackedStateAction); // #2. "Stacked"
    manager.add(fMinimizedStateAction); // #3. "Minimized"
    // manager.add(new Separator()); // separators create a new group
    manager.add(fExpandedObjectStateAction); // #4. "Objects with Tables"
    manager.add(fExpandedStackedStateAction); // #5. "Stacked with Tables"
    manager.add(new Separator());
    manager.add(fObjectCallPathStateAction); // "Focus on Call Path"
  }

  @Override
  protected void configureToolBar(final IToolBarManager manager)
  {
    super.configureToolBar(manager);
    manager.insertBefore(AbstractGraphicalJiveView.ZOOM_CONTROLS_GROUP, new Separator(
    		PathDiagramView.PATH_DIAGRAM_GROUP));
    manager.appendToGroup(PathDiagramView.PATH_DIAGRAM_GROUP, fScrollLockAction);
  }

  @Override
  protected void createActions()
  {
    super.createActions();
    fScrollLockAction = PathStateActionFactory.createScrollLockAction();
    // Create the pull down menu actions
    fMinimizedStateAction = PathStateActionFactory.createMinimizedStateAction();
    fObjectStateAction = PathStateActionFactory.createObjectStateAction();
    fStackedStateAction = PathStateActionFactory.createStackedStateAction();
    fExpandedObjectStateAction = PathStateActionFactory.createObjectExpandedStateAction();
    fExpandedStackedStateAction = PathStateActionFactory.createStackedExpandedStateAction();
    fObjectCallPathStateAction = PathStateActionFactory.createCallPathStateAction();
  }

  @Override
  protected ContextMenuProvider createContextMenuProvider()
  {
    return new PathDiagramContextMenuProvider(getViewer());
  }

  /**for JPF**/
  @Override
	protected EditPartFactory createEditPartFactory()
  {
	//System.out.println("PathDiagramView :: createEditPartFactory()");
    return new PathModelEditPartFactory();
  }

  @Override
  protected GraphicalViewer createGraphicalViewer()
  {
    return new ScrollingGraphicalViewer();
  }

  @Override
  protected String getDefaultContentDescription()
  {
    return "No path diagrams to display at this time.";
  }

  @Override
  protected ImageDescriptor getDisplayDropDownDisabledImageDescriptor()
  {
    return IM_BASE_TREE.disabledDescriptor();
  }

  @Override
  protected ImageDescriptor getDisplayDropDownEnabledImageDescriptor()
  {
    return IM_BASE_TREE.enabledDescriptor();
  }

  @Override
  protected String getDisplayTargetDropDownText()
  {
    return "Display Path Diagram";
  }

  /**
   * An {@code EditPartFactory} used to create {@code EditPart}s for elements in the
   * {@code PathModel}.
   * 
   * @see PathDiagramEditPart
   * @see PathEditPart
   * @see PathConnectionEditPart
   */
  /**for JPF**/
  private static class PathModelEditPartFactory implements EditPartFactory
  {
    /**
     * An empty edit part to be used when there are no targets being debugged.
     */
    private static final EditPart EMPTY_EDIT_PART = new AbstractGraphicalEditPart()
      {
        @Override
        protected void createEditPolicies()
        {
          // TODO Determine if this should be implemented
        }

        @Override
        protected IFigure createFigure()
        {
          //System.out.println("PathDiagramView :: createFigure()");
          return new Figure();
        }
      };

    @Override
    public EditPart createEditPart(final EditPart context, final Object model)
 {
  		//System.out.println("PathDiagramView :: createEditPart(final EditPart context, final Object model)");
			if (model == null) {
				//System.out.println("|||||||||||model instance of NULL");
				return PathModelEditPartFactory.EMPTY_EDIT_PART;
			}
			/** for JPF **/
			if (model instanceof IPathStartEvent) {
				try {
					
					 IPathStartEvent e = (IPathStartEvent)model;
					return PathGraph.mpPEP.get(((IPathStartEvent) model).eventId());

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
			if(model instanceof Long){
				String lb = PathEditPart.pathInfo.instructionCG;
				return new PathConnectionEditPart((Long) model,lb);
			}
			if (model instanceof IJiveDebugTarget) {
				return new PathDiagramEditPart(model);
			}
			throw new IllegalArgumentException("Unknown element type:  "
					+ model.getClass());
		}
  }
}
