package edu.buffalo.cse.jive.internal.ui.view.path.graph;

import org.eclipse.draw2d.graph.Node;

///-import edu.buffalo.cse.jive.model.IPathModel.IPath;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;

/**IPath changed to IPathStartEvent chaged to Long**/
public interface IGraphLayout
{
  public void addPathNode(IPathStartEvent c, Node node);

  public void addEdge(Node v, Node u);

  public Node getNode(IPathStartEvent path);

  public INodePosition getPosition(IPathStartEvent c);

  public void updatePositions();
}