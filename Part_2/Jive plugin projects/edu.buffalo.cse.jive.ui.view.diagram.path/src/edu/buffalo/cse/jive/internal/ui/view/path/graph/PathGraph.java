package edu.buffalo.cse.jive.internal.ui.view.path.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.swt.widgets.Composite;

///-import edu.buffalo.cse.jive.model.IPathModel.IPath;
///-import edu.buffalo.cse.jive.model.IPathModel.IPathMember;
import edu.buffalo.cse.jive.model.IEventModel.EventKind;
import edu.buffalo.cse.jive.model.IEventModel.IInitiatorEvent;
import edu.buffalo.cse.jive.model.IEventModel.IJiveEvent;
import edu.buffalo.cse.jive.model.IEventModel.IPathEndEvent;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
import edu.buffalo.cse.jive.model.IEventModel.ITransaction;
import edu.buffalo.cse.jive.model.IEventModel.OutputFormat;
import edu.buffalo.cse.jive.model.IExecutionModel;
import edu.buffalo.cse.jive.model.IModel.ILineValue;
import edu.buffalo.cse.jive.model.IModel.ILineValue;
///-import edu.buffalo.cse.jive.model.IModel.IPathReference;
import edu.buffalo.cse.jive.model.IModel.IOutOfModelMethodReference;
import edu.buffalo.cse.jive.model.IModel.IThreadValue;
import edu.buffalo.cse.jive.model.IModel.IValue;
import edu.buffalo.cse.jive.model.IStaticModel.NodeModifier;
import edu.buffalo.cse.jive.model.IVisitor;
import edu.buffalo.cse.jive.ui.view.diagram.path.PathDiagramFactory;
import edu.buffalo.cse.jive.ui.view.diagram.path.PathDiagramView;
import edu.buffalo.cse.jive.internal.ui.view.path.editparts.PathEditPart;

/**
 * Representation of the path diagram as a graph.
 */
public class PathGraph
{
  private final IGraphLayout layout;
  public static Map<Long,PathEditPart> mpPEP = new HashMap<Long,PathEditPart>();

  public PathGraph(final IExecutionModel model)
  {
	//System.out.println("PathGraph :: PathGraph(final IExecutionModel model)");
	
	 assert (model != null);
    this.layout = PathDiagramFactory.createLayout();
    model.readLock();
    try
    {
        IPathStartEvent root = model.pathView().lookupRoot();
		layout.addPathNode(root, new Node(root.eventId()));
		mpPEP.put(root.eventId(), new PathEditPart(root));
		addChildren(root, root.children());
        layout.updatePositions();
    }catch(NullPointerException ex){
    	System.out.println("- Not generating Path Diagram");
    	System.out.println("- Provide [-Tree] argument in order to generate Path Diagram");
    }catch(Exception ex){
    	ex.printStackTrace();
    }
    finally
    {
      model.readUnlock();
    }
  }

  private void addChildren(IPathStartEvent parent, List<IPathStartEvent> children) {	
	  
	  for (IPathStartEvent pse : children)
	  {
		  layout.addPathNode(pse, new Node(pse.eventId()));
		  mpPEP.put(pse.eventId(), new PathEditPart(pse));
		  Node u = layout.getNode(parent);	  
		  Node v = layout.getNode(pse);
		  layout.addEdge(u,v);
		  addChildren(pse, pse.children());
	 }
}

public INodePosition getPosition(IPathStartEvent c)
  {
    return layout.getPosition(c);
  }
}