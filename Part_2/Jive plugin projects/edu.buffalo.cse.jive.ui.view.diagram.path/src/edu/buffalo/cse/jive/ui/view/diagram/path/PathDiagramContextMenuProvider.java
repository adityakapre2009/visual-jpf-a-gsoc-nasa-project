package edu.buffalo.cse.jive.ui.view.diagram.path;

import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchActionConstants;

import edu.buffalo.cse.jive.internal.ui.view.path.editparts.PathEditPart;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
///-import edu.buffalo.cse.jive.model.IPathModel.IPath;
import edu.buffalo.cse.jive.ui.IJumpMenuManager;
import edu.buffalo.cse.jive.ui.ISliceMenuManager;
import edu.buffalo.cse.jive.ui.JiveUIPlugin;

public class PathDiagramContextMenuProvider extends ContextMenuProvider
{
  private final IJumpMenuManager jumpMenuFactory;
  private final ISliceMenuManager sliceMenuFactory;

  public PathDiagramContextMenuProvider(final EditPartViewer viewer)
  {
    super(viewer);
    this.jumpMenuFactory = JiveUIPlugin.getDefault().createJumpMenuManager(viewer);
    this.sliceMenuFactory = JiveUIPlugin.getDefault().createSliceMenuManager();
  }

  @Override
  public void buildContextMenu(final IMenuManager manager)
  {
	final EditPartViewer viewer = getViewer();
    final IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
    if (!selection.isEmpty())
    {
      final EditPart part = (EditPart) selection.getFirstElement();
      if (part instanceof PathEditPart)
      {
        if (part.getModel() instanceof IPathStartEvent)
        {
          buildContextMenuFor((IPathStartEvent) part.getModel(), manager);
        }
      }
    }
    manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
  }

  private void buildContextMenuFor(final IPathStartEvent path, final IMenuManager manager)
  {
    jumpMenuFactory.createJumpToMenu(path, manager, true);
  }
}