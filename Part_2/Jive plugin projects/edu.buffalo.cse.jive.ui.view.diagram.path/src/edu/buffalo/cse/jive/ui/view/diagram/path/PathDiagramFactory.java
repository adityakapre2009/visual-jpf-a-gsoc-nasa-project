package edu.buffalo.cse.jive.ui.view.diagram.path;

import org.eclipse.draw2d.IFigure;

import edu.buffalo.cse.jive.internal.ui.view.path.figures.PathFigureFactory;
import edu.buffalo.cse.jive.internal.ui.view.path.graph.PathGraphFactory;
import edu.buffalo.cse.jive.internal.ui.view.path.graph.IGraphLayout;

public enum PathDiagramFactory
{
  HIERARCHICAL,
  TABULAR;
  private static PathDiagramFactory INSTANCE = HIERARCHICAL;

  public static IFigure createPathDigramFigure()
  {
    if (PathDiagramFactory.INSTANCE == HIERARCHICAL)
    {
      return PathFigureFactory.createHierarchicalPathFigure();
    }
    return PathFigureFactory.createTabularPathFigure();
  }

  public static IGraphLayout createLayout()
  {
    if (PathDiagramFactory.INSTANCE == HIERARCHICAL)
    {
      return PathGraphFactory.createHierarchicalLayout();
    }
    return PathGraphFactory.createTabularLayout();
  }

  
  public static void setHierarchical()
  {
	  PathDiagramFactory.INSTANCE = HIERARCHICAL;
  }

  public static void setTabular()
  {
	  PathDiagramFactory.INSTANCE = TABULAR;
  }
}
