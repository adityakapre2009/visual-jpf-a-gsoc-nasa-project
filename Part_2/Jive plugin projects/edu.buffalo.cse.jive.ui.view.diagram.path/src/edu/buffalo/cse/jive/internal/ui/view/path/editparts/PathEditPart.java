package edu.buffalo.cse.jive.internal.ui.view.path.editparts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.NodeListener;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editparts.AbstractTreeEditPart;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;

import edu.buffalo.cse.jive.internal.ui.view.path.figures.PathFigure;
import edu.buffalo.cse.jive.internal.ui.view.path.figures.PathFigure.LabeledPathFigure;
///-import edu.buffalo.cse.jive.model.IPathModel.IPath;
///-import edu.buffalo.cse.jive.model.IPathModel.IPathMember;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
import edu.buffalo.cse.jive.model.IExecutionModel;
///-import edu.buffalo.cse.jive.model.IModel.IPathReference;
import edu.buffalo.cse.jive.model.IModel.IOutOfModelMethodReference;
import edu.buffalo.cse.jive.model.IStaticModel.NodeKind;
import edu.buffalo.cse.jive.model.IVisitor;
import edu.buffalo.cse.jive.ui.IJiveEditPart;

/**
 * An {@code EditPart} serving as a controller for a {@code Path}. It is responsible for creating
 * the appropriate {@code PathFigure} as well as supplying the children and connections for the
 * path.
 * 
 * @see PathDiagramEditPart
 * @see PathConnectionEditPart
 * @see PathFigure
 * @see LabeledPathFigure
 */
public class PathEditPart extends AbstractGraphicalEditPart 
implements IJiveEditPart,NodeEditPart
 {
	private PathState pathState;
	private boolean focusCallPath;
	private final PathState minimizedState;
	private final PathState objectState;
	private boolean showMemberTables;
	private final PathState stackedState;
	public static PathInfo pathInfo = new PathInfo();

	public PathEditPart(final IPathStartEvent model) {
		// System.out.println("PathEditPart :: PathEditPart(final IPathStartEvent model)");
		objectState = PathState.createObjectState(this);
		stackedState = PathState.createStackedState(this);
		minimizedState = PathState.createMinimizedState(this);
		pathState = stackedState;
		setModel(model);
	}

	public IExecutionModel executionModel() {
		final PathDiagramEditPart contents = (PathDiagramEditPart) getViewer()
				.getContents();
		return contents != null ? contents.executionModel() : null;
	}

	public boolean focusCallPath() {
		return focusCallPath;
	}

	@Override
	public ConnectionAnchor getSourceConnectionAnchor(
			final ConnectionEditPart connection) {
		setPathState();
		return pathState.getSourceConnectionAnchor(connection);
	}

	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		// TODO Determine if this should be implemented
		return null;
	}

	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			final ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		// TODO Determine if this should be implemented
		return null;
	}

	public boolean showMemberTables() {
		return showMemberTables;
	}

	private void setPathState() {
		final PathDiagramEditPart contents = (PathDiagramEditPart) getViewer()
				.getContents();
		focusCallPath = contents.isCallPathFocused();
		showMemberTables = false;
		switch (contents.getPathState()) {
		case MINIMIZED:
			pathState = minimizedState;
			break;
		case OBJECTS:
			pathState = objectState;
			break;
		case OBJECTS_MEMBERS:
			showMemberTables = true;
			pathState = objectState;
			break;
		case STACKED:
			pathState = stackedState;
			break;
		case STACKED_MEMBERS:
			showMemberTables = true;
			pathState = stackedState;
			break;
		default:
			pathState = stackedState;
		}
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE,
				new NonResizableEditPolicy());
	}

	@Override
	protected IFigure createFigure() {
		// System.out.println("PathEditPart :: createFigure() :: "+getModel());
		setPathState();
		return pathState.createFigure();
	}

	/**
	 * Returns the {@code PathEditPart} associated with the supplied path.
	 * 
	 * @param path
	 *            the path whose edit part will be returned
	 * @return the path edit part for the path, or <code>null</code> if none
	 *         exist
	 */
	protected PathEditPart getPathEditPart(final IPathStartEvent path) {
		return (PathEditPart) getViewer().getEditPartRegistry().get(path);
	}

	/**
	 * Returns the {@code PathDiagramEditPart} for the edit part. This
	 * corresponds to the contents of the {@code RootEditPart}.
	 * 
	 * @return the path diagram edit part of the edit part
	 */
	protected PathDiagramEditPart getDiagramEditPart() {
		return (PathDiagramEditPart) getRoot().getContents();
	}

	/** if commented - children not seen **/
	@Override
	protected List<IPathStartEvent> getModelChildren() {
		// //System.out.println("PathEditPart :: getModelChildren()");
		final IPathStartEvent path = (IPathStartEvent) getModel();
		return path.children();

	}

	/**
	 * Returns a list of variable instances that reference some path other than
	 * this edit part's path. Note that variable instances are unique, as
	 * opposed to the actual reference values, which are not necessarily unique
	 * and, therefore, may be cached by the underlying factory.
	 */
	@Override
	protected List<Long> getModelSourceConnections() {
		final IPathStartEvent sourcePath = (IPathStartEvent) getModel();
		// System.out.println("PathEditPart :: getModelTargetConnections() : "+sourcePath.eventId());
		List<Long> sourceFor = new ArrayList<Long>();

		/**
		 * Traverse this path's variable members and, if some variable's value
		 * references another path, this edit part's path (source) must
		 * "point to" the path (target). Hence, this edit part is a source for
		 * all such (target) values. The respective variable instance is thus
		 * added to the list.
		 */
		try {
			for (IPathStartEvent target : sourcePath.children()) {
				sourceFor.add(target.eventId());
			}
			pathInfo.instructionCG = sourcePath.instructionCG();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sourceFor;
	}

	/**
	 * Returns a list of variable instances that reference this edit part's
	 * path. Note that variable instances are unique, as opposed to the actual
	 * reference values, which are not necessarily unique and, therefore, may be
	 * cached by the underlying factory.
	 * 
	 * TODO takes O(n) to determine targets-- provide a model service to perform
	 * this as a lookup
	 */
	@Override
	protected List<Long> getModelTargetConnections() {
		// System.out.println("PathEditPart :: getModelSourceConnections() - "+targetPath.eventId());
		final IPathStartEvent targetPath = (IPathStartEvent) getModel();

		final List<Long> targetFor = new LinkedList<Long>();
		/**
		 * Otherwise, traverse each of the path's variable members and, if value
		 * represents this edit part's path, the path (source) must "point to"
		 * this edit part's path (target). Hence, the edit part is a target for
		 * all such (source) values.
		 */
		targetFor.add(targetPath.eventId());
		return targetFor;
	}

	@Override
	protected void refreshVisuals() {
		setPathState();
		pathState.refreshVisuals();
	}

	/**
	 * Adds a child {@code EditPart} for the supplied {@code Path} if one does
	 * not already exist.
	 * 
	 * @param path
	 *            the child path
	 */
	void addChildPath(final IPathStartEvent path) {
		// System.out.println("PathEditPart :: addChildPath(final IPathStartEvent path)");
		/**
		 * Create a new child edit part if one does not already exist. Instance
		 * paths for an object are created together, so during normal execution
		 * this case will not be reached. However, while replaying recorded
		 * states it will.
		 */
		if (getPathEditPart(path) == null) {

			final EditPart childPart = createChild(path);
			addChild(childPart, -1);
		}
	}

	/**
	 * Removes the {@code EditPart} associated with the supplied {@code Path} if
	 * one exists. For {@code MethodPath}s, connections are first updated to
	 * reflect the fact that the path has been removed from the model.
	 * 
	 * @param path
	 *            the child path
	 */
	void removeChildPath(final IPathStartEvent path) {
		// Remove the edit part if it exists
		final EditPart childPart = getPathEditPart(path);
		if (childPart != null) {
			removeChild(childPart);
		}
	}

}