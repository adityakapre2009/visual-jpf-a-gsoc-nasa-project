package edu.buffalo.cse.jive.importer.jpf;
//people tech
//greenline
//akamai
import java.io.*;
import java.util.StringTokenizer;

import org.w3c.dom.*;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

public class XmlConverter {

	static Document doc;
	static boolean start = true;

	public static void generateXML(String url) throws IOException {

		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			Element root = doc.createElement("events");
			doc.appendChild(root);
			FileInputStream fstream = new FileInputStream(url);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			XMLDataVO vo = null;
			while ((strLine = br.readLine()) != null) {
				strLine = strLine.trim();
				if (strLine.equals("event")) {
					vo = new XMLDataVO();
				} else if (strLine.startsWith("id")) {
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setId(str);
				} else if (strLine.startsWith("thread")) {
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setThread(str);
				} else if (strLine.startsWith("kind")) {
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setKind(str);
				} else if (strLine.startsWith("line")) {
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setLine(Integer.parseInt(str));
				} else if (strLine.startsWith("timestamp")) {
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setTimestamp(str);
				} else if (strLine.startsWith("file")) {
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setFile(str);
				} else if (strLine.contains("type")) {
					/** +2 because we need to skip 'L' **/
					/** -1 is to avoid the semicolon **/
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length() - 1);
					vo.setBlIsDetails(true);
					vo.setType(str);
				} else if (strLine.startsWith("object")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setObject(Long.parseLong(str));
				} else if (strLine.startsWith("newthread")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setNewthread(str);
				} else if (strLine.startsWith("caller")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setCaller(str);
				} else if (strLine.startsWith("target")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setTarget(str);
				} else if (strLine.startsWith("signature")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setSignature(str);
				} else if (strLine.startsWith("action")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setAction(str);
				} else if (strLine.startsWith("choiceNumber")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setChoiceNumber(str);
				} else if (strLine.startsWith("totalNumOfChoices")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setTotalNumOfChoices(str);
				} else if (strLine.startsWith("sourceLocation")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setSourceLocation(str);
				} else if (strLine.startsWith("nextChoice")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setNextChoice(str);
				} else if (strLine.startsWith("choiceType")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setChoiceType(str);
				} else if (strLine.startsWith("parentid")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setParentId(str);
				} else if (strLine.startsWith("thisid")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setThisId(str);
				} else if (strLine.startsWith("varToBeChanged")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setVarToBeChanged(str);
				} else if (strLine.startsWith("returner")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setReturner(str);
				} else if (strLine.startsWith("elements")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setElements(str);
				} else if (strLine.startsWith("value")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setValue(str);
				} else if (strLine.startsWith("field")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setField(str);
				} else if (strLine.startsWith("context")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setContext(str);
				} else if (strLine.startsWith("thrower")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setThrower(str);
				} else if (strLine.startsWith("framePopped")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setFramePopped(str);
				} else if (strLine.startsWith("exception")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setException(str);
				} else if (strLine.startsWith("th_name")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setTh_name(str);
				} else if (strLine.startsWith("instructionCG")) {
					vo.setBlIsDetails(true);
					String str = strLine.substring(strLine.indexOf("=") + 1,
							strLine.length());
					vo.setInstruction(str);
				} else if (strLine.startsWith("endevent")) {
					createXmlTree(vo, root);
				}

			}
			doneXML();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void createXmlTree(XMLDataVO vo, Element root) {

		Element child = doc.createElement("event");
		root.appendChild(child);

		Element child12 = doc.createElement("id");
		Text text = doc.createTextNode(vo.getId());
		child12.appendChild(text);
		child.appendChild(child12);

		Element child1 = doc.createElement("thread");
		Text text1 = doc.createTextNode(vo.getThread().toString());
		child1.appendChild(text1);
		child.appendChild(child1);

		Element child2 = doc.createElement("kind");
		Text text2 = doc.createTextNode(vo.getKind());
		child2.appendChild(text2);
		child.appendChild(child2);

		Element child3 = doc.createElement("file");
		Text text3 = doc.createTextNode(vo.getFile());
		child3.appendChild(text3);
		child.appendChild(child3);

		Element child4 = doc.createElement("line");
		Text text4 = doc.createTextNode("" + vo.getLine() + "");
		child4.appendChild(text4);
		child.appendChild(child4);

		if (vo.getStatus() != null) {
			Element child11 = doc.createElement("status");
			Text text11 = doc.createTextNode(vo.getStatus());
			child11.appendChild(text11);
			child.appendChild(child11);
		}

		if (vo.getTimestamp() != null) {
			Element child5 = doc.createElement("timestamp");
			Text text5 = doc.createTextNode("" + System.currentTimeMillis()
					+ "");
			child5.appendChild(text5);
			child.appendChild(child5);
		}

		Element child6 = null;
		if (vo.isBlIsDetails()) {
			vo.setBlIsDetails(false);
			child6 = doc.createElement("details");

			if (vo.getAction() != null) {
				Element child8 = doc.createElement("action");
				Text text8 = doc.createTextNode("" + vo.getAction() + "");
				child8.appendChild(text8);
				child6.appendChild(child8);
				child.appendChild(child6);
			}
			if (vo.getParentId() != null) {
				Element child15 = doc.createElement("parentid");
				Text text15 = doc.createTextNode("" + vo.getParentId() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getThisId() != null) {
				Element child15 = doc.createElement("thisid");
				Text text15 = doc.createTextNode("" + vo.getThisId() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getNextChoice() != null) {
				Element child14 = doc.createElement("nextChoice");
				Text text14 = doc.createTextNode("" + vo.getNextChoice() + "");
				child14.appendChild(text14);
				child6.appendChild(child14);
				child.appendChild(child6);
			}
			if (vo.getVarToBeChanged() != null) {
				Element child9 = doc.createElement("varToBeChanged");
				Text text9 = doc.createTextNode("" + vo.getVarToBeChanged()
						+ "");
				child9.appendChild(text9);
				child6.appendChild(child9);
				child.appendChild(child6);
			}
			if (vo.getChoiceNumber() != null) {
				Element child9 = doc.createElement("choiceNumber");
				Text text9 = doc.createTextNode("" + vo.getChoiceNumber() + "");
				child9.appendChild(text9);
				child6.appendChild(child9);
				child.appendChild(child6);
			}
			if (vo.getTotalNumOfChoices() != null) {
				Element child10 = doc.createElement("totalNumOfChoices");
				Text text10 = doc.createTextNode("" + vo.getTotalNumOfChoices()
						+ "");
				child10.appendChild(text10);
				child6.appendChild(child10);
				child.appendChild(child6);
			}
			if (vo.getSourceLocation() != null) {
				Element child13 = doc.createElement("sourceLocation");
				Text text13 = doc.createTextNode("" + vo.getSourceLocation()
						+ "");
				child13.appendChild(text13);
				child6.appendChild(child13);
				child.appendChild(child6);
			}
			if (vo.getChoiceType() != null) {
				Element child15 = doc.createElement("choiceType");
				Text text15 = doc.createTextNode("" + vo.getChoiceType() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getType() != null) {
				Element child7 = doc.createElement("type");
				Text text7 = doc.createTextNode(vo.getType());
				child7.appendChild(text7);
				child6.appendChild(child7);
				child.appendChild(child6);
			}
			if (vo.getObject() != 0L) {
				Element child7 = doc.createElement("object");
				Text text7 = doc.createTextNode("" + vo.getObject() + "");
				child7.appendChild(text7);
				child6.appendChild(child7);
				child.appendChild(child6);
			}
			if (vo.getNewthread() != null) {
				Element child7 = doc.createElement("newthread");
				Text text7 = doc.createTextNode(vo.getNewthread());
				child7.appendChild(text7);
				child6.appendChild(child7);
				child.appendChild(child6);
			}
			if (vo.getCaller() != null) {
				Element child7 = doc.createElement("caller");
				CDATASection cdata = doc.createCDATASection(vo.getCaller());
				child7.appendChild(cdata);
				child6.appendChild(child7);
				child.appendChild(child6);
			}
			if (vo.getTarget() != null) {
				Element child7 = doc.createElement("target");
				CDATASection cdata = doc.createCDATASection(vo.getTarget());
				child7.appendChild(cdata);
				child6.appendChild(child7);
				child.appendChild(child6);
			}
			if (vo.getSignature() != null) {
				Element child7 = doc.createElement("signature");
				CDATASection cdata = doc.createCDATASection(vo.getSignature());
				child7.appendChild(cdata);
				child6.appendChild(child7);
				child.appendChild(child6);
			}
			if (vo.getReturner() != null) {
				Element child7 = doc.createElement("returner");
				CDATASection cdata = doc.createCDATASection(vo.getReturner());
				child7.appendChild(cdata);
				child6.appendChild(child7);
				child.appendChild(child6);
			}
			if (vo.getElements() != null) {
				Element child7 = doc.createElement("elements");
				Text text7 = doc.createTextNode("" + vo.getElements() + "");
				child7.appendChild(text7);
				child6.appendChild(child7);
				child.appendChild(child6);
			}
			if (vo.getValue() != null) {
				Element child15 = doc.createElement("value");
				Text text15 = doc.createTextNode("" + vo.getValue() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getField() != null) {
				Element child15 = doc.createElement("field");
				Text text15 = doc.createTextNode("" + vo.getField() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getContext() != null) {
				Element child15 = doc.createElement("context");
				Text text15 = doc.createTextNode("" + vo.getContext() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getThrower() != null) {
				Element child15 = doc.createElement("thrower");
				Text text15 = doc.createTextNode("" + vo.getThrower() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getFramePopped() != null) {
				Element child15 = doc.createElement("framePopped");
				Text text15 = doc.createTextNode("" + vo.getFramePopped() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getException() != null) {
				Element child15 = doc.createElement("exception");
				Text text15 = doc.createTextNode("" + vo.getException() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getTh_name() != null) {
				Element child15 = doc.createElement("th_name");
				Text text15 = doc.createTextNode("" + vo.getTh_name() + "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
			if (vo.getInstructionCG() != null) {
				Element child15 = doc.createElement("instructionCG");
				Text text15 = doc.createTextNode("" + vo.getInstructionCG()
						+ "");
				child15.appendChild(text15);
				child6.appendChild(child15);
				child.appendChild(child6);
			}
		}
	}

	/*
	 * public static void doneXML() { try { DOMImplementationLS domImplementation =
	 * (DOMImplementationLS) doc .getImplementation(); LSSerializer lsSerializer =
	 * domImplementation.createLSSerializer(); String str =
	 * lsSerializer.writeToString(doc); LSOutput lso =
	 * domImplementation.createLSOutput(); lso.setEncoding("utf-8");
	 * 
	 * File file = new File("interface.xml"); if (file.exists()) { file.delete(); }
	 * FileOutputStream fos = new FileOutputStream(file); lso.setByteStream(fos);
	 * lsSerializer.write(doc, lso); } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * }
	 */
	
	public static void doneXML() {
		try { 
			DOMSource source = new DOMSource(doc);
		    FileWriter writer = new FileWriter(new File("interface.xml"));
		    StreamResult result = new StreamResult(writer);

		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
		    Transformer transformer = transformerFactory.newTransformer();
		    transformer.transform(source, result);
		    
		} catch (Exception e) { 
			e.printStackTrace(); 
		}
		
	}
	
	
	public static void main(String[] args) {
		try {
			generateXML("/home/adi/jpf-platform/jpf-core/jive.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}