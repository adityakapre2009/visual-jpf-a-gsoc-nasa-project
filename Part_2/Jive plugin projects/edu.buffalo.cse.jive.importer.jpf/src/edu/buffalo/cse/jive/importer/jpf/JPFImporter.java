package edu.buffalo.cse.jive.importer.jpf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
//import org.eclipse.jdt.core.IMember;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import edu.buffalo.cse.jive.launch.offline.IOfflineImporter;
//import edu.buffalo.cse.jive.launch.offline.IOfflineImporter;
//import edu.buffalo.cse.jive.launch.offline.OfflineImporterException;
import edu.buffalo.cse.jive.model.IContourModel.IContextContour;
import edu.buffalo.cse.jive.model.IContourModel.IContourMember;
import edu.buffalo.cse.jive.model.IContourModel.IMethodContour;
import edu.buffalo.cse.jive.model.IContourModel.IObjectContour;
import edu.buffalo.cse.jive.model.IEventModel.EventKind;
import edu.buffalo.cse.jive.model.IEventModel.IJiveEvent;
import edu.buffalo.cse.jive.model.IEventModel.IMethodExitEvent;
import edu.buffalo.cse.jive.model.IEventModel.IMethodTerminatorEvent;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
import edu.buffalo.cse.jive.model.IExecutionModel;
import edu.buffalo.cse.jive.model.IModel.ILineValue;
import edu.buffalo.cse.jive.model.IModel.IThreadValue;
import edu.buffalo.cse.jive.model.IModel.IValue;
import edu.buffalo.cse.jive.model.IStaticModel.IMethodNode;
import edu.buffalo.cse.jive.model.IStaticModel.ITypeNode;
import edu.buffalo.cse.jive.model.IStaticModel.ITypeNodeRef;
import edu.buffalo.cse.jive.model.IStaticModel.NodeKind;
import edu.buffalo.cse.jive.model.IStaticModel.NodeModifier;
import edu.buffalo.cse.jive.model.IStaticModel.NodeOrigin;
import edu.buffalo.cse.jive.model.IStaticModel.NodeVisibility;
import edu.buffalo.cse.jive.model.factory.IContourFactory;
import edu.buffalo.cse.jive.model.factory.IEventFactory;
import edu.buffalo.cse.jive.model.factory.IStaticModelFactory;
import edu.buffalo.cse.jive.model.factory.IStaticModelFactory.IStaticModelDelegate;
import edu.buffalo.cse.jive.model.factory.IValueFactory;
import edu.buffalo.cse.jive.model.lib.Tools;
import edu.buffalo.cse.jive.model.lib.XMLTools.XMLEventField;

//JAXP
//SAX

public class JPFImporter extends DefaultHandler implements IOfflineImporter {
	private final static int BASE_16 = 16;
	private final static int BLOCK_SIZE = 100;
	private final static String INIT_ESCAPED = "<init>";
	private static final String PROTO_JPF = "jpf://";
	private EventDAO event;
	private List<EventDAO> eventList;
	private String fileName;
	private IExecutionModel model;
	private IStaticModelDelegate staticModelDelegate;
	private Map<Long, String> threadToScope;
	private Map<Long, LinkedList<String>> threadToScopeStack;
	private Map<Long, String> objectIdToScope;
	private Map<String, Set<IObjectContour>> scopeToContours;
	public static int noOfEvents = 0;
	boolean isTree = false;
	List<IJiveEvent> all = new ArrayList<IJiveEvent>();

	public JPFImporter() {

	}

	@Override
	public void characters(final char[] ch, final int start, final int length)
			throws SAXException {
		if (event != null && event.currentField() != null) {
			event.setCurrentFieldValue(unescape(new String(ch, start, length)));
		}
	}

	public void createEvents() {

		List<IJiveEvent> jpfEvents = Tools.newArrayList(eventList.size());
		final List<IJiveEvent> jiveEvents = Tools
				.newArrayList(eventList.size());
		noOfEvents = jiveEvents.size();

		// keep track of all stacks
		final Map<IThreadValue, Stack<IMethodContour>> stacks = Tools
				.newHashMap();
		for (final EventDAO event : eventList) {
			// all events reference a timestamp
			final long timestamp = event.timestamp();
			final IThreadValue thread = createThread(event);
			final ILineValue line = valueFactory().createUnavailableLine();
			if (event.kind() == null) {
				System.err.println("SKIPPING EVENT" + event.fields);
				continue;
			}
			try {
				switch (event.kind()) {
				case TYPE_LOAD: {

				}
					break;
				case VAR_ASSIGN: {

					final String starget = event
							.getFieldValue(XMLEventField.CONTEXT);
					final String key = event.getFieldValue(
							XMLEventField.SIGNATURE).replace(";/", ";.");
					final String typeKey = key.substring(0,
							key.indexOf(';') + 1);
					final ITypeNode typeNode = resolveType(typeKey);
					final String methodKey = key.substring(0,
							key.indexOf(')') + 1);
					IMethodNode methodNode = staticFactory().lookupMethodNode(
							methodKey);

					IContextContour contour = contourFactory()
							.lookupStaticContour(typeNode.name());
					IValue value = valueFactory().createPrimitiveValue(
							XMLEventField.VALUE.toString());
					methodNode.addDataMember("aditya", 0, 0, typeNode,
							NodeOrigin.NO_JIVE,
							Collections.<NodeModifier> emptySet(),
							NodeVisibility.NV_LOCAL, value);
					final IMethodContour method = contour.createMethodContour(
							methodNode, thread);
					createStaticContours(typeNode, timestamp, thread, line,
							jiveEvents);
					IContourMember member = method.lookupMember("aditya");
					jiveEvents.add(eventFactory().createVarAssignEvent(thread,
							line, value, member));
				}
					break;
				case EXCEPTION_THROW: {
					String strException = event
							.getFieldValue(XMLEventField.EXCEPTION);
					IValue exception = valueFactory().createPrimitiveValue(
							strException);
					String strThrower = event
							.getFieldValue(XMLEventField.THROWER);
					IValue thrower = valueFactory().createPrimitiveValue(
							strThrower);
					String bool = event
							.getFieldValue(XMLEventField.FRAME_POPPED);
					boolean wasFramePopped = Boolean.getBoolean(bool);
					jiveEvents.add(eventFactory().createExceptionThrowEvent(
							thread, line, exception, thrower, true));
				}
					break;
				case PATH_START: {

					String id = event.id.toString();
					final String nextChoice = event
							.getFieldValue(XMLEventField.NEXTCHOICE);
					final String totalNumOfChoices = event
							.getFieldValue(XMLEventField.TOTALNUMOFCHOICES);
					final String choiceType = event
							.getFieldValue(XMLEventField.CHOICETYPE);
					final String sourceLocation = event
							.getFieldValue(XMLEventField.SOURCELOCATION);
					final String parentId = event
							.getFieldValue(XMLEventField.PARENTID);
					final String varToBeChanged = event
							.getFieldValue(XMLEventField.VARTOBECHANGED);
					final String thisId = event
							.getFieldValue(XMLEventField.THISID);
					final String instructionCG = event
							.getFieldValue(XMLEventField.INSTRUCTIONCG);

					if (isTree)
						jpfEvents.add(eventFactory().createPathStartEvent(
								thread, line, id, parentId, nextChoice,
								totalNumOfChoices, sourceLocation, choiceType,
								varToBeChanged, thisId, instructionCG));
					else
						jiveEvents.add(eventFactory().createPathStartEvent(
								thread, line, id, parentId, nextChoice,
								totalNumOfChoices, sourceLocation, choiceType,
								varToBeChanged, thisId, instructionCG));

				}
					break;
				case PATH_END: {
					String id = event.id.toString();
					final String nextChoice = event
							.getFieldValue(XMLEventField.NEXTCHOICE);
					final String totalNumOfChoices = event
							.getFieldValue(XMLEventField.TOTALNUMOFCHOICES);
					final String choiceType = event
							.getFieldValue(XMLEventField.CHOICETYPE);
					final String sourceLocation = event
							.getFieldValue(XMLEventField.SOURCELOCATION);
					final String parentId = event
							.getFieldValue(XMLEventField.PARENTID);
					final String thisId = event
							.getFieldValue(XMLEventField.THISID);

					if (isTree)
						jpfEvents.add(eventFactory().createPathEndEvent(thread,
								line, id, parentId, nextChoice,
								totalNumOfChoices, sourceLocation, choiceType,
								thisId));
					else
						jiveEvents.add(eventFactory().createPathEndEvent(
								thread, line, id, parentId, nextChoice,
								totalNumOfChoices, sourceLocation, choiceType,
								thisId));

				}
					break;
				case FIELD_WRITE: {
					System.out.println("JPFImporter.java**************************************FIELD_WRITE : id="+event.id);
					final String typeName = event
							.getFieldValue(XMLEventField.TYPE);
					final ITypeNode typeNode = resolveType(typeName);
					System.out.println("- value = "+event
							.getFieldValue(XMLEventField.VALUE));
					if(typeNode==null){
						System.out.println("- type node IS NULL!!!");
					}else{
						System.out.println("- TYPE NODE RESOLVED : kind = "+typeNode.kind());
					}

					if (typeNode.kind() == NodeKind.NK_ARRAY
							|| typeNode.kind() == NodeKind.NK_CLASS) {

						final long objectId = Long.parseLong(
								event.getFieldValue(XMLEventField.TARGET),
								JPFImporter.BASE_16);
						long oid = objectId;
						//Contour name resolution does not require L+typeName
						final IContextContour contour = contourFactory()
								.lookupInstanceContour(typeName, oid);
						IValue newValue = valueFactory().createPrimitiveValue(
								event.getFieldValue(XMLEventField.VALUE));
						if (contour != null) {
							System.out.println("- CONTOUR RESOLVED : kind = "+contour.kind());
							if (event.getFieldValue(XMLEventField.TYPE)
									.contains("[]")) {
								int field = Integer.parseInt(event
										.getFieldValue(XMLEventField.FIELD));
								IContourMember member = contour
										.lookupMember(field);
								jiveEvents.add(eventFactory()
										.createFieldWriteEvent(thread, line,
												contour, newValue, member));

							} else {
								String field = event
										.getFieldValue(XMLEventField.FIELD);
								IContourMember member = contour
										.lookupMember(field);
								if(member!=null)
								jiveEvents.add(eventFactory()
										.createFieldWriteEvent(thread, line,
												contour, newValue, member));
							}
						} else {
							System.out.println("CONTOUR IS NULL");
						}

					}
					System.out.println("JPFImporter.java**************************************FIELD_WRITE : END");
				}
					break;
				case METHOD_CALL: {

					System.out.println("JPFImporter.java**************************************METHOD_CALL : id="+event.id);
					final String scaller = event
							.getFieldValue(XMLEventField.CALLER);
					final IValue caller;
					if ("SYSTEM".equals(scaller)
							|| stacks.get(thread).isEmpty()) {
						caller = valueFactory().createSystemCaller();
					} else {
						// top of the respective thread
						final IMethodContour top = stacks.get(thread).peek();
						caller = valueFactory().createReference(top);
					}
					final String starget = event
							.getFieldValue(XMLEventField.TARGET);
					final IValue target;
					if ("SYSTEM".equals(starget)) {
						target = valueFactory().createSystemCaller();
					} else {
						final String key = event.getFieldValue(
								XMLEventField.SIGNATURE).replace(";/", ";.");
						final String typeKey = key.substring(0,
								key.indexOf(';'));
						String str = staticFactory()
								.typeNameToSignature(typeKey);
						System.out.println("- typeKey = "+typeKey);
						System.out.println("- str = "+str);
						final ITypeNode typeNode = resolveType(typeKey);
						if(typeNode==null){
							System.out.println("- typeNode NOT found");
						}
						
						
						createStaticContours(typeNode, timestamp, thread, line,
								jiveEvents);
						
						final IContextContour callerContour = contourFactory()
								.lookupStaticContour(typeNode.name());
						
						
						String returnTypeKey = key.indexOf(')') == -1 ? "V"
								: key.substring(key.indexOf(')') + 1,
										key.length());
						if (!returnTypeKey.startsWith("L")
								&& returnTypeKey.endsWith(";")) {
							returnTypeKey = returnTypeKey.substring(0,
									returnTypeKey.length() - 1);
						}
						
						System.out.println("- returnTypeKey = "+returnTypeKey);
						ITypeNode returnTypeNode = resolveType(returnTypeKey);
						returnTypeNode = returnTypeNode == null ? staticFactory()
								.lookupVoidType() : returnTypeNode;
						createStaticContours(returnTypeNode, timestamp, thread,
								line, jiveEvents);
						final String targetName = starget.substring(starget
								.indexOf('#') + 1);
						 String methodKey = key.substring(0,
								key.indexOf(')') + 1);
						
						
						String methodNode_key = "L"+methodKey;
						System.out.println("- methodNode_key = "+methodNode_key);
						IMethodNode methodNode = staticFactory().lookupMethodNode(methodNode_key);
						if (methodNode == null) {
							System.out.println("- method not found");
							methodNode = typeNode.addMethodMember(
									key.substring(0, key.indexOf(')') + 1),
									targetName, -1, -1, returnTypeNode,
									NodeOrigin.NO_JIVE,
									Collections.<NodeModifier> emptySet(),
									NodeVisibility.NV_PUBLIC,
									Collections.<ITypeNodeRef> emptySet());
						}else{
							System.out.println("- method node found for : "+methodKey);
						}
						final IMethodContour method = callerContour
								.createMethodContour(methodNode, thread);
						stacks.get(thread).push(method);
						target = valueFactory().createReference(method);
					}
					jiveEvents.add(eventFactory().createRTMethodCallEvent(
							timestamp, thread, line, caller, target));
					System.out.println("JPFImporter.java**************************************METHOD_CALL : END");
				}
					break;
				case OBJECT_NEW: {
					System.out.println("JPFImporter.java**************************************OBJECT_NEW : id="+event.id);
					final String typeName = event
							.getFieldValue(XMLEventField.TYPE);
					final ITypeNode typeNode = resolveType(typeName);
					if (typeNode == null) {
						System.out.println("- TYPE NODE IS NULL");
					}else{
						System.out.println("- TYPPE NODE FOUND = "+typeNode.key());
					}
					createStaticContours(typeNode, timestamp, thread, line,
							jiveEvents);
					final long objectId = Long.parseLong(
							event.getFieldValue(XMLEventField.OBJECT),
							JPFImporter.BASE_16);
					long oid = objectId;
					
					IContextContour existing = contourFactory()
							.lookupInstanceContour(typeName, oid);
					
					
					/*while (existing != null) {
						oid = oid * 7 + (oid % 19);
						existing = contourFactory().lookupInstanceContour(
								typeName, oid);
					}*/
					
					int length = -1;
					if (event.getFieldValue(XMLEventField.ELEMENTS) != null) {
						length = Integer.parseInt(event
								.getFieldValue(XMLEventField.ELEMENTS));
					}
					final IObjectContour contour = length == -1 ? typeNode
							.createInstanceContour(oid) : typeNode
							.createArrayContour(oid, length);
					//final String scope = threadToScope.get(thread.id());
					jiveEvents.add(eventFactory().createNewObjectEvent(thread,
							line, contour));
					System.out.println("JPFImporter.java**************************************OBJECT_NEW : END");
				}
					break;
				case THREAD_CREATE: {
					System.out.println("JPFImporter.java**************************************THREAD_CREATE : id="+event.id);
					IObjectContour threadContour = contourFactory()
							.lookupInstanceContour(
									"java.lang.Thread",
									Long.valueOf(event
											.getFieldValue(XMLEventField.NEWTHREAD)));
					if (threadContour != null) {
						break;
					}
					final ITypeNode threadNode = resolveType("Ljava/lang/Thread;");
					createStaticContours(threadNode, timestamp, thread, line,
							jiveEvents);
					threadContour = threadNode.createInstanceContour(Integer
							.valueOf(event
									.getFieldValue(XMLEventField.NEWTHREAD)));
					stacks.put(createThread(Long.valueOf(event
							.getFieldValue(XMLEventField.NEWTHREAD))),
							new Stack<IMethodContour>());
					jiveEvents.add(eventFactory().createRTThreadNewEvent(
							timestamp,
							thread,
							line,
							threadContour,
							Long.valueOf(event
									.getFieldValue(XMLEventField.NEWTHREAD))));
					final IValue caller;
					if (stacks.get(thread) == null
							|| stacks.get(thread).isEmpty()) {
						caller = valueFactory().createSystemCaller();
					} else {
						final IMethodContour top = stacks.get(thread).peek();
						caller = valueFactory().createReference(top);
					}
					final IValue target = valueFactory().createReference(
							threadContour.createMethodContour(threadNode
									.methodMembers().get(0), thread));
					// create the constructor call
					jiveEvents.add(eventFactory().createMethodCallEvent(thread,
							line, caller, target));
					jiveEvents.add(eventFactory().createMethodEnteredEvent(
							thread, line));
					jiveEvents
							.add(eventFactory()
									.createFieldWriteEvent(
											thread,
											line,
											threadContour,
											valueFactory()
													.createPrimitiveValue(
															event.getFieldValue(XMLEventField.NEWTHREAD)),
											threadContour.lookupMember("id")));
					jiveEvents
							.add(eventFactory()
									.createFieldWriteEvent(
											thread,
											line,
											threadContour,
											valueFactory()
													.createPrimitiveValue(
															event.getFieldValue(XMLEventField.TH_NAME)),
											threadContour.lookupMember("name")));
					jiveEvents.add(eventFactory().createMethodExitEvent(thread,
							line));
					System.out.println("JPFImporter.java**************************************THREAD_CREATE : END");

				}
					break;
				case THREAD_END: {
					jiveEvents.add(eventFactory().createRTThreadEndEvent(
							timestamp, thread));
				}
					break;
				case THREAD_SLEEP: {
					final long waketime = Long.valueOf(event
							.getFieldValue(XMLEventField.WAKETIME));
					jiveEvents.add(eventFactory().createRTThreadSleepEvent(
							timestamp, thread, waketime));
				}
					break;
				case THREAD_START: {

				}
					break;
				case THREAD_WAKE: {
					final long waketime = Long.valueOf(event
							.getFieldValue(XMLEventField.WAKETIME));
					jiveEvents.add(eventFactory().createRTThreadWakeEvent(
							timestamp, thread, waketime));
				}
					break;
				case THREAD_YIELD: {
					final long waketime = Long.valueOf(event
							.getFieldValue(XMLEventField.WAKETIME));
					jiveEvents.add(eventFactory().createRTThreadYieldEvent(
							timestamp, thread, waketime));
				}
					break;
				case MONITOR_LOCK_BEGIN: {
				}
					break;
				case SYSTEM_END: {
					jiveEvents.add(eventFactory().createRTSystemExitEvent(
							timestamp));
				}
					break;
				case SYSTEM_START: {
					jiveEvents.add(eventFactory().createRTSystemStartEvent(
							timestamp));
				}
					break;
				case METHOD_ENTERED: {
					jiveEvents.add(eventFactory().createMethodEnteredEvent(
							thread, line));
				}
					break;
				case METHOD_EXIT: {
					if (!stacks.get(thread).isEmpty()) {
						Object o = stacks.get(thread).pop();
						IMethodExitEvent methodExit = (IMethodExitEvent) eventFactory()
								.createMethodExitEvent(thread, line);
						IMethodTerminatorEvent terminator = methodExit;
						jiveEvents.add(methodExit);
						jiveEvents.add(eventFactory()
								.createMethodReturnedEvent(terminator));
					}

				}
					break;
				case THREAD_PRIORITY: {
				}
					break;
				case MONITOR_LOCK_END: {
				}
					break;
				case MONITOR_LOCK_FAST: {
				}
					break;
				case MONITOR_RELOCK: {
				}
					break;
				case MONITOR_UNLOCK_BEGIN: {
				}
					break;
				case MONITOR_UNLOCK_COMPLETE: {
				}
					break;
				case MONITOR_UNLOCK_END: {
				}
					break;
				case MONITOR_UNLOCK_FAST: {
				}
					break;
				case SCOPE_ALLOC: {
				}
					break;
				case SCOPE_BACKING_ALLOC: {
				}
					break;
				case SCOPE_BACKING_FREE: {
				}
					break;
				case SCOPE_ENTER: {
				}
					break;
				case SCOPE_EXIT: {
				}
					break;
				case SCOPE_FREE: {
				}
					break;
				case SCOPE_POP: {
				}
					break;
				case SCOPE_PUSH: {
				}
					break;
				default: {
				}
				}
			} catch (final RuntimeException e) {
				e.printStackTrace();
				throw e;
			}

			if (jiveEvents.size() == JPFImporter.BLOCK_SIZE) {
				if (!isTree) {
					all.addAll(jiveEvents);
					jiveEvents.clear();
				}
			}

		}

		if (isTree) {
			model.eventOccurred(null, jpfEvents);
			jiveEvents.clear();
			jpfEvents.clear();

		} else if (jiveEvents.size() > 0) {
			all.addAll(jiveEvents);
			PathFinder pf = new PathFinder(all);
			IPathStartEvent last_ps = pf.getLastPathStart();
			List<IJiveEvent> ls = pf.getEventsToThisChoice(last_ps);
			model.eventOccurred(null, ls);
			jiveEvents.clear();
			jpfEvents.clear();
			ls.clear();
		}
	}

	@Override
	public void endElement(final String uri, final String localName,
			final String qName) throws SAXException {

		if (checkEvent(localName)) {
			eventList.add(event);
			event = null;
		} else if (event != null && event.currentField() != null
				&& localName.equalsIgnoreCase(event.currentField().fieldName())) {
			event.setCurrentField(null);
		}
	}

	// @Override
	public void process(final String url, final IExecutionModel model,
			final IStaticModelDelegate upstream) {

		try {
			// generates the xml file at this location
			String file_path = null;
			if (url.contains("-Tree")) {
				isTree = true;
				file_path = url.substring(0, url.indexOf("-Tree"));
			} else if (url.contains(" ")) {
				file_path = url.substring(0, url.indexOf(" "));
			} else {
				file_path = url;
			}
			XmlConverter.generateXML(file_path.substring(JPFImporter.PROTO_JPF
					.length()));
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		final long start = System.nanoTime();
		this.event = null;
		this.eventList = Tools.newArrayList();

		// name the xml file as interface.xml
		this.fileName = "interface.xml";
		this.model = model;
		this.staticModelDelegate = upstream;
		this.threadToScope = Tools.newHashMap();
		this.threadToScopeStack = Tools.newHashMap();
		this.objectIdToScope = Tools.newHashMap();
		this.scopeToContours = Tools.newHashMap();
		try {
			final SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(true);
			factory.setNamespaceAware(true);
			final SAXParser parser = factory.newSAXParser();
			parser.parse(fileName, this);
			createEvents();
		} catch (final FactoryConfigurationError e) {
			e.printStackTrace();

		} catch (final Exception e) {
			e.printStackTrace();

		}
		System.err.println("Imported the JPF trace in "
				+ ((System.nanoTime() - start) / 1000000) + "ms.");

	}

	@Override
	public void startElement(final String uri, final String localName,
			final String name, final Attributes atts) throws SAXException {
		// skip the root tag
		if (checkRoot(localName)) {
			event = null;
			return;
		}
		// skip the event tag
		if (checkEvent(localName)) {
			event = new EventDAO();
			return;
		}
		// skip the details tag
		if (checkDetails(localName)) {
			event.setCurrentField(null);
			return;
		}
		// try to resolve the tag to a known XML event field tag
		event.setCurrentField(checkEventField(localName));
	}

	private boolean checkDetails(final String tagName) {
		return "details".equalsIgnoreCase(tagName);
	}

	private boolean checkEvent(final String tagName) {
		return "event".equalsIgnoreCase(tagName);
	}

	private XMLEventField checkEventField(final String tagName) {
		try {
			return XMLEventField.valueOf(tagName.toUpperCase());
		} catch (final IllegalArgumentException e) {
		}
		return null;
	}

	private boolean checkRoot(final String tagName) {
		return "events".equalsIgnoreCase(tagName);
	}

	private String constructorName(final String signature) {
		String result = staticFactory().signatureToTypeName(signature);
		result = result.substring(result.lastIndexOf('.') + 1);
		result = result.substring(result.lastIndexOf('$') + 1);
		return result;
	}

	private IContourFactory contourFactory() {
		return model.contourFactory();
	}

	/**
	 * just checks if static contour exists, if yes, it will not create new,
	 * otherwise it will create a new one
	 **/
	private void createStaticContours(final ITypeNode typeNode,
			final long timestamp, final IThreadValue thread,
			final ILineValue line, final List<IJiveEvent> jiveEvents) {
		if (typeNode == null || typeNode.kind() != NodeKind.NK_CLASS) {
			return;
		}
		// static contour of the loaded type
		IContextContour contour= contourFactory().lookupStaticContour(typeNode.name());
		if (contour == null) {

			// create a new static contour for the super class
			if (typeNode.superClass() != null
					&& typeNode.superClass().node() != null) {

				createStaticContours(typeNode.superClass().node(), timestamp,
						thread, line, jiveEvents);
			}
			// safety: all parent static contours exist and have corresponding
			// load events
			contour = typeNode.createStaticContour();
			// add the type load event
			jiveEvents.add(eventFactory().createRTTypeLoadEvent(timestamp,
					thread, line, contour));
		}
	}

	private IThreadValue createThread(final EventDAO event) {
		return "SYSTEM".equals(event.threadId()) ? createThread(-1000)
				: createThread(Integer.valueOf(event.threadId()));
	}

	private IThreadValue createThread(final long threadId) {
		return threadId == -1000 ? valueFactory().createThread(-1000, "SYSTEM")
				: valueFactory().createThread(threadId, "Thread-" + threadId);
	}

	private IEventFactory eventFactory() {
		return model.eventFactory();
	}

	private ITypeNode resolveType(final String key) {
		// primitive types
		/*if (!key.startsWith("L") && !key.startsWith("[")) {
			return staticFactory().lookupTypeNode(
					key.indexOf(';') >= 0 ? key.substring(0, key.indexOf(';'))
							: key);
		}*/
		// find the type node
		ITypeNode typeNode = staticFactory().lookupTypeNode(key);
		if (typeNode == null) {
			if ("Ljava/lang/Thread;".equals(key)) {
				typeNode = staticFactory().createThreadNode();
			} else {
			/*	typeNode = staticModelDelegate.resolveType(key,
						staticFactory().signatureToTypeName(key)).node();
			*/
				typeNode = staticModelDelegate.resolveType(
						staticFactory().typeNameToSignature(key),key).node();
			
			}
		}
	//	System.out.println("- created type node  = "+typeNode);
		return typeNode;
	}

	private IStaticModelFactory staticFactory() {
		return model.staticModelFactory();
	}

	private String unescape(final String value) {
		String string = value;
		final int index = string.indexOf(JPFImporter.INIT_ESCAPED);
		if (index > 0) {
			string = string.substring(0, index)
					+ constructorName(string.substring(0, index - 1))
					+ string.substring(index
							+ JPFImporter.INIT_ESCAPED.length());
		}
		return string;
	}

	private IValueFactory valueFactory() {
		return model.valueFactory();
	}

	private static class EventDAO {
		private XMLEventField currentField;
		private final Map<XMLEventField, String> fields;
		private final Map<String, EventKind> kindLookup;
		private String file;
		private Long id;
		private EventKind kind;
		private Integer line;
		private String threadId;
		private Long timestamp;

		EventDAO() {
			this.fields = Tools.newHashMap();
			this.kindLookup = Tools.newHashMap();
			for (final EventKind kind : EventKind.values()) {
				kindLookup.put(kind.eventName(), kind);
			}
		}

		public XMLEventField currentField() {
			return this.currentField;
		}

		public String getFieldValue(final XMLEventField field) {
			return fields.get(field);
		}

		public boolean hasAttribute(final XMLEventField field) {
			return fields.containsKey(field);
		}

		public EventKind kind() {
			return this.kind;
		}

		public void setCurrentField(final XMLEventField value) {
			this.currentField = value;
			this.fields.put(currentField, null);
		}

		public void setCurrentFieldValue(final String value) {
			if (currentField == null) {
				throw new IllegalArgumentException(
						"Cannot set the value of a null event field.");
			}
			switch (currentField) {
			case ID:
				this.id = Long.valueOf(value);
				break;
			case KIND:
				this.kind = kindLookup.get(value);
				break;
			case FILE:
				this.file = value;
				break;
			case LINE:
				this.line = Integer.valueOf(value);
				break;
			case THREAD:
				this.threadId = value;
				break;
			case TIMESTAMP:
				this.timestamp = Long.valueOf(value);
				break;
			default:
				this.fields.put(currentField, value);
				break;
			}
		}

		public String threadId() {
			return this.threadId;
		}

		public long timestamp() {
			return this.timestamp;
		}

		@Override
		public String toString() {
			final StringBuffer buffer = new StringBuffer("");
			buffer.append("id = ").append(id).append("\n");
			buffer.append("timestamp = ").append(timestamp).append("\n");
			buffer.append("kind = ").append(kind.toString()).append("\n");
			buffer.append("file = ").append(file).append("\n");
			buffer.append("line = ").append(line).append("\n");
			buffer.append("threadId = ").append(threadId).append("\n");
			for (final XMLEventField field : fields.keySet()) {
				if (field == null) {
					continue;
				}
				buffer.append(field.fieldName()).append(" = ")
						.append(fields.get(field)).append("\n");
			}
			return buffer.toString();
		}
	}
	
	public static void main(String[] args) {
		final IOfflineImporter importer = createImporter(url);
		System.out.println("Importing from URL : " + url);
		if (importer == null) {
			System.out.println("importer null");
			System.out.println("False"); 
		}
		try {
			final IStaticModelDelegate downstream = new IStaticModelDelegate() {
				@Override
				public IMethodNode resolveMethod(final ITypeNode type, final Object methodObject,
						final String methodKey) {
					// no need for method resolution
					return null;
				}

				@Override
				public ITypeNodeRef resolveType(final String typeKey, final String typeName) {
					final IStaticModelFactory factory = model.staticModelFactory();
					return factory.createTypeNode(typeKey, typeName, factory.lookupRoot(), -1, -1,
							typeKey.startsWith("[") ? NodeKind.NK_ARRAY : NodeKind.NK_CLASS, NodeOrigin.NO_JIVE,
							Collections.<NodeModifier>emptySet(), NodeVisibility.NV_PUBLIC, factory.lookupObjectType(),
							Collections.<ITypeNodeRef>emptySet(), model.valueFactory().createNullValue());

				}

				@Override
				public void setUpstream(final IStaticModelDelegate upstream) {
					// no need for upstream
				}
			};
			final IStaticModelDelegate upstream = ASTFactory.createStaticModelDelegate(model, project, null,
					downstream);
			System.out.println("project --------------> = " + project);

			importer.process(url, model, upstream);
			
		} catch (final Exception e) {
			e.printStackTrace();
		}
		
	}
}
