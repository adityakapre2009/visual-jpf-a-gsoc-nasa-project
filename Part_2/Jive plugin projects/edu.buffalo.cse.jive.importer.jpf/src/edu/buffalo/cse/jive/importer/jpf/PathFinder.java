package edu.buffalo.cse.jive.importer.jpf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import edu.buffalo.cse.jive.model.IExecutionModel;
import edu.buffalo.cse.jive.model.IModel;
import edu.buffalo.cse.jive.model.IEventModel.EventKind;
import edu.buffalo.cse.jive.model.IEventModel.IJiveEvent;
import edu.buffalo.cse.jive.model.IEventModel.IPathStartEvent;
import edu.buffalo.cse.jive.model.IEventModel.EventKind;
import edu.buffalo.cse.jive.model.lib.Tools;


public class PathFinder {

	public static List<? extends IJiveEvent> eventList;

	public PathFinder(List<IJiveEvent> jiveEvents) {

		eventList = Tools.newArrayList(JPFImporter.noOfEvents);
		this.eventList = jiveEvents;
	}

	public IPathStartEvent getLastPathStart() {

		IPathStartEvent last_IPS = null;
		try {
			for (IJiveEvent je : eventList) {
				if (je.kind() == EventKind.PATH_START) {
					last_IPS = (IPathStartEvent) je;
				}
			}
			return last_IPS;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

	}

	public List<IJiveEvent> getEventsToThisChoice(IPathStartEvent ps) {

		IPathStartEvent choice = ps;
		Stack<Long> stack = new Stack<Long>();
		stack.push(choice.eventId());
		while (true) {
			IPathStartEvent parent = getParentEvent(choice);
			if (parent == null) {
				break;
			}
			if (parent.parentId() == -1) {
				stack.push(parent.eventId());
				break;
			} else {
				stack.push(parent.eventId());
				choice = parent;
			}
		}
		List<IJiveEvent> ls_JumpTo = getAllKindsOfEvents(stack);
		return ls_JumpTo;
	}

	List<IJiveEvent> getAllKindsOfEvents(Stack<Long> stack) {
		long iEnd = 0;
		List<? extends IJiveEvent> ls = eventList;
		boolean isToBeAdded = true;
		List<IJiveEvent> ls_JumpTo = new ArrayList<IJiveEvent>();
		for (IJiveEvent ee : ls) {
			if (ee.kind() == EventKind.PATH_START) {

				if (stack.isEmpty()) {
					System.out.println("Stack empty at new path start = "
							+ ee.eventId());
					break;
				}

				IPathStartEvent pse = (IPathStartEvent) ee;
				Long pse_stack_id = stack.peek();
				if (pse.eventId() == pse_stack_id) {

					isToBeAdded = true;
					stack.pop();

				} else {
					isToBeAdded = false;
				}
			}

			if (isToBeAdded) {
				if (ee.kind() != EventKind.PATH_START) {
					ls_JumpTo.add(ee);
				}
			} else if (ee.kind() == EventKind.FIELD_WRITE) {
				ls_JumpTo.add(ee);
			}
		}
		return ls_JumpTo;
	}

	IPathStartEvent getParentEvent(IPathStartEvent pse) {
		try {
			List<? extends IJiveEvent> ls = eventList;
			for (IJiveEvent e : ls) {
				if (e instanceof IPathStartEvent) {
					IPathStartEvent p = (IPathStartEvent) e;
					if (pse.parentId() == p.eventId()) {
						return p;
					}
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}