package edu.buffalo.cse.jive.importer.jpf;

public class XMLDataVO {

	String id;
	String thread;
	String kind;
	String file;
	int line;
	String timestamp;
	String details;
	String status;

	/* sub parts of details */
	String priority;
	String newthread;
	String type;
	boolean blIsDetails;
	long object;
	String caller;
	String target;
	String signature;
	String returner;
	String elements;
	String value;
	String field;
	String context;
	String thrower;
	String framePopped;
	String exception;
	String th_name;

	/** tree events **/
	String action;
	String choiceNumber;
	String totalNumOfChoices;
	String sourceLocation;
	String nextChoice;
	String choiceType;
	String parentId;
	String thisId;
	String varToBeChanged;
	String instructionCG;

	public String getInstructionCG() {
		return instructionCG;
	}

	public void setInstruction(String instructionCG) {
		this.instructionCG = instructionCG;
	}

	public String getTh_name() {
		return th_name;
	}

	public void setTh_name(String th_name) {
		this.th_name = th_name;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getFramePopped() {
		return framePopped;
	}

	public void setFramePopped(String framePopped) {
		this.framePopped = framePopped;
	}

	public String getThrower() {
		return thrower;
	}

	public void setThrower(String thrower) {
		this.thrower = thrower;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getElements() {
		return elements;
	}

	public void setElements(String elements) {
		this.elements = elements;
	}

	public String getReturner() {
		return returner;
	}

	public void setReturner(String returner) {
		this.returner = returner;
	}

	public String getThisId() {
		return this.thisId;
	}

	public void setThisId(String thisId) {
		this.thisId = thisId;
	}

	public String getVarToBeChanged() {
		return varToBeChanged;
	}

	public void setVarToBeChanged(String varToBeChanged) {
		this.varToBeChanged = varToBeChanged;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChoiceType() {
		return choiceType;
	}

	public void setChoiceType(String choiceType) {
		this.choiceType = choiceType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getChoiceNumber() {
		return choiceNumber;
	}

	public void setChoiceNumber(String choiceNumber) {
		this.choiceNumber = choiceNumber;
	}

	public String getTotalNumOfChoices() {
		return totalNumOfChoices;
	}

	public void setTotalNumOfChoices(String totalNumOfChoices) {
		this.totalNumOfChoices = totalNumOfChoices;
	}

	public String getSourceLocation() {
		return sourceLocation;
	}

	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}

	public String getNextChoice() {
		return nextChoice;
	}

	public void setNextChoice(String nextChoice) {
		this.nextChoice = nextChoice;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public long getObject() {
		return object;
	}

	public void setObject(long object) {
		this.object = object;
	}

	public boolean isBlIsDetails() {
		return blIsDetails;
	}

	public void setBlIsDetails(boolean blIsDetails) {
		this.blIsDetails = blIsDetails;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNewthread() {
		return newthread;
	}

	public void setNewthread(String newthread) {
		this.newthread = newthread;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	/*
	 * public int getId() { return id; } public int setId(String id) { this.id =
	 * id; }
	 */

	public String getThread() {
		return thread;
	}

	public void setThread(String thread) {
		this.thread = thread;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}