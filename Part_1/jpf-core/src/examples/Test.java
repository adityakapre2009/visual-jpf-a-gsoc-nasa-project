class Tree {
 public Tree(int n) { value = n; left = null; right = null;}; 

 public void insert(int n) {
      if (value == n) return;
      if (value < n) 
     if (right == null) right = new Tree(n);
         else right.insert(n);
      else if (left == null) left = new Tree(n);
           else left.insert(n);
 } 
 protected int value;
 protected Tree left;
 protected Tree right;
}

public class Test { 
    public static void main (String args[]) {
          Tree tr;
           tr = new Tree(100);
           tr.insert(50);
           tr.insert(150);
           tr.insert(25);
           tr.insert(75);
           tr.insert(125);
           tr.insert(175);
       }
}