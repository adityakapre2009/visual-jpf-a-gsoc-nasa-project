/** 
	Date: 20/05/2011
	PROGRAM: ProducerConsumerTest.java (Producer-Consumer implementation)
	CLASSES: Producer, Consumer, Buffer, ProducerConsumerTest
	DESCRIPTION: Buffer implements put and get methods. Producer puts an item
	and sleeps for a random time (<100 ms) and put an item. Consumer grabs this.
	The Producer generates an integer between 0 and 9 (inclusive), stores it in 
	a Buffer object, and prints the generated number. The Producer sleeps for 
	a random amount of time between 0 and 100 milliseconds before repeating the 
	number generating cycle. 
	The Consumer consumes all integers from the Buffer as quickly as they 
	become available.
	The 'synchronized' keyword ensures mutual exclusion. i.e. only one thread
	can access Buffer object at a time. The two threads must also be able to 
	notify one another when they've done their job to ensure synchronization.

	SAFETY CONDITIONS:
	Producer puts an integer to the variable "contents". And Consumer reads the 
	value. Both sychnronize using the condition variable "available".
	(1) If "available" is false, Consumer waits until Producer puts a value to
	"contents" and sets "available" to true.
	(2) If "available" is true, Producer waits until Consumer reads the value 
	in "contents" and sets "avaiable" to false.

	LIVENESS CONDITIONS:
	Both the threads notify each other as soon as they produce or consume an
	item allowing the other to take control.

	TRY_1: Remove "synchronized" keyword for get() and put() methods 
	RESULT: The program through IllegalMonitorStateException during execution.
	CONCLUSION: 'synchronized' keyword ensures mutual exclusion. i.e. only one
	thread can access Buffer object at a time.
	REASON: Monitors (a programming language concept) is implemented in Java. 
	This guards against removing synchronized. In order to use notify* or wait, 
	the current thread needs to be the owner of the lock on the object, in this 
	case the 'this' object.  Either declare the function as 'synchronized' or 
	create a synchronized block around the notify.
	CONCLUSION: Try removing synchronized, wait() and notify() to simulate race
	condition.

	UPDATE: Included comments from the following website
	http://www.csse.uwa.edu.au/programming/javatut/essential/threads/synchronization.html

	HOW TO SIMULATE RACE CONDITION?
	TRY_2: Remove "synchronized" keyword for get() and put() methods + remove
	statements involving wait() and notifyAll() - i.e. no conditions are checked.  
	These will essentially remove mutual exclusion and synchronization.
	RESULT: Both Producer and Consumer threads are not synchornized now. Producer,
	when scheduled continuously, overwrites the value. Consumer misses out all 
	those values and get only the last value. Consumer, when scheduled continuously,
	consumes the same value. 

*/

public class ProducerConsumerTest {
    public static void main(String[] args) {
        Buffer c = new Buffer();
        Producer2 p1 = new Producer2(c, 1);
        Consumer2 c1 = new Consumer2(c, 1);

        p1.start();
        c1.start();
		//p1.start();
    }
}

/** Note that the method declarations for both put & get contain the synchronized 
	keyword. Hence, the system associates a unique lock with every instance of 
	Buffer (including the one shared by the Producer and the Consumer). 
	Whenever control enters a synchronized method, the thread that called the 
	method locks the object whose method has been called. Other threads cannot 
	call a synchronized method on the same object until the object is unlocked.
*/

 class Buffer {
    private int contents;
    private boolean available = false;

	/** When the Consumer calls the Buffer's get method, it locks the
		Buffer, thereby preventing Producer to call Buffer's put method.
		When get method returns, the Consumer unlocks the Buffer.
	*/
    public synchronized int get() {
        while (available == false) {
            try {
                wait();
            } catch (InterruptedException e) { }
        }
        available = false;
        notifyAll();
        return contents;
    }

	/** When the Producer calls the Buffer's put method, it locks the
		Buffer, thereby preventing Consumer to call Buffer's get method.
		When put method returns, the Producer unlocks the Buffer.
	*/
    public synchronized void put(int value) {
        while (available == true) {
            try {
                wait();
            } catch (InterruptedException e) { }
        }
        contents = value;
        available = true;
        notifyAll();
    }
}

class Producer2 extends Thread {
    private Buffer Buffer;
    private int number;

    public Producer2(Buffer c, int number) {
        Buffer = c;
        this.number = number;
    }

    public void run() {
        for (int i = 0; i < 3; i++) {
            Buffer.put(i);
            System.out.println("Producer #" + this.number
                               + " put: " + i);
            /*try {
                sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) { } */
        }
    }
}

class Consumer2 extends Thread {
    private Buffer Buffer;
    private int number;

    public Consumer2(Buffer c, int number) {
        Buffer = c;
        this.number = number;
    }

    public void run() {
        int value = 0;
        for (int i = 0; i < 3; i++) {
            value = Buffer.get();
            System.out.println("Consumer #" + this.number
                               + " got: " + value);
        }
    }
}


