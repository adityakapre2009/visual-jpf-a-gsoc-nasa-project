/*
	FILE: PCMain-Race.java (Producer-Consumer with bounded buffer and NO monitor)
	CLASSES: PCNoMonitor, Producer, Consumer, PCMain
	DESCRIPTION: PCNoMonitor class implements a queue of size specified by MAXQUEUE.
	It also declares an integer variable count which maintains the current 
	length of the queue. Methods putMessage() and getMessage() add and retrieve
	an item to and from the queue respectively and increments/decrements count.
	
	NOTE that this is a special case of PCMain.java to reproduce race conditions.
	Following changes have been done:
	1. 'synchronized' keyword is removed from both put and get methods. 
	2. Both do not implement wait() and notify() methods. 
	3. sleep() is added instead of wait()
	4. To explicitly observe the errors, the actual queue size [got using size()] 
	and the count are printed together. In few cases, differences can be seen.
	
	Capture the output and grep for patterns "1, 0", "2, 1", ......, and 
	"0, 1", "1, 2" ...... to see such instances.
	>> java PCMainRace > output
	>> grep "1, 0" output OR vi output and search for these patterns

 */

import java.util.Vector;
import java.util.NoSuchElementException;

class PCNoMonitor {	

	static final int MAXQUEUE = 5;
	static int count = 0;
	private Vector messages = new Vector();

	/**	putMessage() is called by Producer. First it checks for the size of the 
		queue and as long as the size is less than the MAXQUEUE, it keeps adding 
		message to the queue. Else,	it waits. Once it adds a message, it notifies.
	*/

	public void putMessage() throws InterruptedException {
		System.out.println("count, actual size = " + count + ", " + messages.size() );
		while ( count == MAXQUEUE )  // SAFETY_CONDITION_1
			Thread.sleep(1);	// wait() removed. Instead it sleeps for 1 ms
		messages.addElement( new java.util.Date().toString() );
		count++;
		System.out.println(", after adding, count, actual size = " + count + ", " + messages.size() );
		if ( count > MAXQUEUE )
			throw new ArrayIndexOutOfBoundsException();
		//notify();	// LIVENESS_CONDITION_1
	}

	/**	getMessage() is called by Consumer. First it notifies the Producer, since
		Consumer can retrieve only if Producer has put any message. Also, before 
		it gets a message, it checks if the queue is empty. If so it waits. 
		Else it keeps retrieving the messages.	
	*/

	public String getMessage() throws InterruptedException {
		//notify();  // LIVENESS_CONDITION_2
		System.out.println();
		System.out.print("count, actual size = " + count + ", " + messages.size() );
		while ( count == 0 )  // SAFETY_CONDITION_2
			Thread.sleep(1);	// wait() removed. Instead it sleeps for 1 ms
		String message = null;
		message = (String)messages.firstElement();
		messages.removeElement( message );
		count--;
		System.out.print(", after retrieving, count, actual size = " + count + ", " + messages.size() );
		return message;
	}
}

class Producer implements Runnable {
	PCNoMonitor monitor;

	Producer(PCNoMonitor m) {
		monitor = m;
	}
	
	public void run() {
		try {
			monitor.putMessage();
			monitor.putMessage();
			monitor.putMessage();
			monitor.putMessage();
			monitor.putMessage();
			/*while ( true ) {
				monitor.putMessage();
				//Thread.sleep( 1000 );
			}*/
		}
		catch( InterruptedException e ) { }
	}
}

class Consumer implements Runnable {
	PCNoMonitor monitor;

	Consumer(PCNoMonitor m) {
		monitor = m;
	}

	public void run() {
		try {
		String message = monitor.getMessage();
		 message = monitor.getMessage();
		 message = monitor.getMessage();
		 message = monitor.getMessage();
		 message = monitor.getMessage();
		}catch(Exception ex){
			
		}
		/*try {
			while ( true ) {
				String message = monitor.getMessage();
				//System.out.println("Got message: " + message);
				//Thread.sleep( 2000 );
			}
		}
		catch( InterruptedException e ) { }*/
	}
}

class PCMainRace {

	public static void main(String args[]) {
		PCNoMonitor monitor = new PCNoMonitor();

		Producer producer = new Producer(monitor);
		new Thread(producer).start();

		Consumer consumer = new Consumer(monitor);
		new Thread(consumer).start();
	}
}
